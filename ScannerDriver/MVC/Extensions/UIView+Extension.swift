//
//  UIView+Extension.swift
//  DBSouqMain
//
//  Created by Illyas Pasha on 10/06/19.
//  Copyright © 2019 Illyas Pasha. All rights reserved.
//

import UIKit

//class LineView : UIView {
//
//    /// An array of optional UIColors (clearColor is used when nil is provided) defining the color of each segment.
//    var colors : [UIColor?] = [UIColor?]() {
//        didSet {
//            self.setNeedsDisplay()
//        }
//    }
//
//    /// An array of CGFloat values to define how much of the view each segment occupies. Should add up to 1.0.
//    var values : [CGFloat] = [CGFloat]() {
//        didSet {
//            self.setNeedsDisplay()
//        }
//    }
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        backgroundColor = UIColor.clear
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//
//    override func draw(_ rect: CGRect) {
//
//        let r = self.bounds // the view's bounds
//        let numberOfSegments = values.count // number of segments to render
//
//        let ctx = UIGraphicsGetCurrentContext() // get the current context
//
//        var cumulativeValue:CGFloat = 0 // store a cumulative value in order to start each line after the last one
//        for i in 0..<numberOfSegments {
//            
//            ctx!.setFillColor(colors[i]?.cgColor ?? UIColor.clear.cgColor) // set fill color to the given color if it's provided, else use clearColor
////            ctx!.fill(CGRect(x: 0, y: cumulativeValue*r.size.height, width: r.size.width, height: values[i]*r.size.height)) // fill that given segment
//
//            //cumulativeValue*r.size.height
//            //
//            
//            ctx!.fill(CGRect(x:cumulativeValue*r.size.height , y: 0, width:values[i]*r.size.width, height:r.size.height ))
//            cumulativeValue += values[i] // increment cumulative value
//        }
//    }
//}




extension UIView
{

    func lineFunction(percentage:Double,color : UIColor,otherColor : UIColor? = .gray){
        //self.frame =  CGRect(x:5, y: 100, width:widthy, height:lenthy)
        let x = 100-percentage
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
        self.backgroundColor = .systemPink
        self.fillColors([color,otherColor!], withPercentage: [percentage,x], direction: .horizontal)
       // self.addSubview(lineView)
    }
    
enum GradientDirection {
    case horizontal
    case vertical
}

func fillColors(_ colors: [UIColor], withPercentage percentages: [Double], direction: GradientDirection = .horizontal) {
    let gradientLayer = CAGradientLayer()
    gradientLayer.frame = bounds
    var colorsArray: [CGColor] = []
    var locationsArray: [NSNumber] = []
    var total = 0.0
    locationsArray.append(0.0)
    for (index, color) in colors.enumerated() {
        // append same color twice
        colorsArray.append(color.cgColor)
        colorsArray.append(color.cgColor)
        // Calculating locations w.r.t Percentage of each
        if index + 1 < percentages.count {
            total += percentages[index]
            let location = NSNumber(value: total / 100)
            locationsArray.append(location)
            locationsArray.append(location)
        }
    }
    locationsArray.append(1.0)
    if direction == .horizontal {
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
    } else {
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
    }
    gradientLayer.colors = colorsArray
    gradientLayer.locations = locationsArray
    backgroundColor = .clear
    layer.addSublayer(gradientLayer)
}
    }


//extension UIView{
//    
//    @IBInspectable var cornerRadius: Double {
//        get {
//            return Double(self.layer.cornerRadius)
//        }set {
//            self.layer.cornerRadius = CGFloat(newValue)
//        }
//    }
//    @IBInspectable var borderWidth: Double {
//        get {
//            return Double(self.layer.borderWidth)
//        }
//        set {
//            self.layer.borderWidth = CGFloat(newValue)
//        }
//    }
//    @IBInspectable var borderColor: UIColor? {
//        get {
//            return UIColor(cgColor: self.layer.borderColor!)
//        }
//        set {
//            self.layer.borderColor = newValue?.cgColor
//        }
//    }
//    @IBInspectable var shadowColor: UIColor? {
//        get {
//            return UIColor(cgColor: self.layer.shadowColor!)
//        }
//        set {
//            self.layer.shadowColor = newValue?.cgColor
//        }
//    }
//    @IBInspectable var shadowOpacity: Float {
//        get {
//            return self.layer.shadowOpacity
//        }
//        set {
//            self.layer.shadowOpacity = newValue
//        }
//    }
//    
//    
//}
extension UIView{
    
    func cornerRadiusMethod(_ conradius: Int,opacity : Double? = 0.6,offx : Int? = 2,offy : Int? = 4,shwRadius : Int? = 0,sahdowColor : UIColor? = .lightGray)  {
        self.layer.cornerRadius = CGFloat(conradius)
       // self.layer.shadowRadius = CGFloat(shwRadius!)
        self.layer.shadowOffset = CGSize(width: offx!, height: offy!)
        self.layer.shadowColor = sahdowColor?.cgColor
       // self.layer.shadowRadius = 4
        
        if let opac = opacity{
            self.layer.shadowOpacity = Float(opac)
        }
        else{
            self.layer.shadowOpacity = 0.4
        }
      
    }
    
}
extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
extension UIView{
    func animShow(){
        self.center.y += self.bounds.height
        UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.center.y -= self.bounds.height
                        //self.center.y = self.center.y
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
       // self.center.y -= self.bounds.height
    }
    func animHide(){
        UIView.animate(withDuration: 1, delay: 0, options: [.curveLinear],
                       animations: {
                        self.center.y += self.bounds.height
                        self.layoutIfNeeded()
                        
        },  completion: {(_ completed: Bool) -> Void in
            self.isHidden = true
        })
    }
}


extension UIView{
    func blink() {
        self.alpha = 0.2
        UIView.animate(withDuration: 0.15, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {self.alpha = 1.0}, completion: nil)
    }
}

extension UIView {
 
    func setGradientBackground(colorOne: UIColor, colorTwo: UIColor) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    
}


extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 1.0, delay: TimeInterval = 0.5, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    
    
}
extension UIView {

/**
 Simply zooming in of a view: set view scale to 0 and zoom to Identity on 'duration' time interval.

 - parameter duration: animation duration
 */
func zoomIn(duration: TimeInterval = 0.4) {
    self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
    UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
        self.transform = CGAffineTransform.identity
    }) { (animationCompleted: Bool) -> Void in
    }
}

 
func zoomOut(duration: TimeInterval = 0.2) {
    self.transform = CGAffineTransform.identity
    UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
        self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
    }) { (animationCompleted: Bool) -> Void in
    }
}

func zoomInWithEasing(duration: TimeInterval = 0.2, easingOffset: CGFloat = 0.2) {
    let easeScale = 1.0 + easingOffset
    let easingDuration = TimeInterval(easingOffset) * duration / TimeInterval(easeScale)
    let scalingDuration = duration - easingDuration
    UIView.animate(withDuration: scalingDuration, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
        self.transform = CGAffineTransform(scaleX: easeScale, y: easeScale)
    }, completion: { (completed: Bool) -> Void in
        UIView.animate(withDuration: easingDuration, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
            self.transform = CGAffineTransform.identity
        }, completion: { (completed: Bool) -> Void in
        })
    })
}

func zoomOutWithEasing(duration: TimeInterval = 0.1, easingOffset: CGFloat = 0.1) {
    let easeScale = 1.0 + easingOffset
    let easingDuration = TimeInterval(easingOffset) * duration / TimeInterval(easeScale)
    let scalingDuration = duration - easingDuration
    UIView.animate(withDuration: easingDuration, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
        self.transform = CGAffineTransform(scaleX: easeScale, y: easeScale)
    }, completion: { (completed: Bool) -> Void in
        UIView.animate(withDuration: scalingDuration, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
            self.transform = CGAffineTransform.identity
        }, completion: { (completed: Bool) -> Void in
        })
    })
}
}
@IBDesignable
class GradientView: UIView {

    override class var layerClass: AnyClass { return CAGradientLayer.self }

    private var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }

    @IBInspectable var color1: UIColor = .orange { didSet { updateColors() } }
    @IBInspectable var color2: UIColor = Theme.appRedColor!  { didSet { updateColors() } }

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        configureGradient()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureGradient()
    }

    private func configureGradient() {
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        updateColors()
    }

    private func updateColors() {
        gradientLayer.colors = [color1.cgColor, color2.cgColor]
    }

}
