//
//  UITableView.swift
//  ProjectTemplate
//
//  Created by vamsi on 24/03/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

extension UITableView {
    func scrollToBottom(animated: Bool) {
        let y = contentSize.height - frame.size.height
        setContentOffset(CGPoint(x: 0, y: (y<0) ? 0 : y), animated: animated)
    }
}

extension UITableView{
    func register(_ cell:UITableViewCell.Type){
        self.register(UINib(nibName: cell.reuseid, bundle: nil), forCellReuseIdentifier: cell.reuseid)
    }
}
