//
//  UIDevice.swift
//  ProjectTemplate
//
//  Created by vamsi on 27/02/21.
//  Copyright © 2021 vamsi. All rights reserved.
//

import Foundation
import UIKit
//

struct Device {
    // iDevice detection code
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE = UIDevice.current.userInterfaceIdiom == .phone
}

extension UIDevice {
    enum DeviceType: String {
        case iPhone4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8_SE2 = "iPhone 6, iPhone 6S, iPhone 7, iPhone 8 or iphone SE2"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX_iPhoneXs_11Pro = "iPhone X , iPhoneXs or iPhone 11Pro"
        case iPhoneXR_11 = "iPhone XR or iphone11"
        case iPhoneXsMax = "iPhone XsMax"
        case iPhone12_12Pro = "iPhone 12_ iPhone 12Pro"
        case iPhone12ProMax = "iPhone 12ProMax"
        case iPhone12Mini = "iPhone 12Mini"
        case ipad1_2_mini = "ipad1 or ipad2 or ipadmini"
        case ipad7 = "ipad7"
        case ipad3_4_Mini5_5_6_9 = "ipad3 , ipad4 ,ipadMini5 , ipad5 , ipad6 or ipad9.7"
        case ipadPro10_ipadAir3 = "ipadPro10.5 or ipadAir3"
        case ipadPro12 = "ipadPro12.9"
        case ipadPro11 = "ipadPro11"
        case unknown = "iPadOrUnknown"
    }
    
    var deviceType: DeviceType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8_SE2
        case 1920,2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 1792:
            return .iPhoneXR_11
        case 2436:
            return .iPhoneX_iPhoneXs_11Pro
        case 2688:
            return .iPhoneXsMax
        case 2532:
            return .iPhone12_12Pro
        case 2340:
            return .iPhone12Mini
        case 2778:
            return .iPhone12ProMax
        case 1024:
            return .ipad1_2_mini
        case 2048:
            return .ipad3_4_Mini5_5_6_9
        case 2160:
            return .ipad7
        case 2224:
            return .ipadPro10_ipadAir3
        case 2388:
            return .ipadPro11
        case 2732:
            return .ipadPro12
        default:
            return .unknown
        }
    }
}

