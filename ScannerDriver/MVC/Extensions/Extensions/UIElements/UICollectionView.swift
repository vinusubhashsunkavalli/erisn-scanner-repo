//
//  UICollectionView.swift
//  ProjectTemplate
//
//  Created by vamsi on 02/04/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

extension UICollectionView{
    func register(_ cell:UICollectionViewCell.Type){
        self.register(UINib(nibName: cell.reuseid, bundle: nil), forCellWithReuseIdentifier: cell.reuseid)
    }
}
