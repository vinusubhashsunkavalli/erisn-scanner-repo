//
//  UILabel.swift
//  ProjectTemplate
//
//  Created by vamsi on 21/03/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

extension UILabel {
    var numberOfVisibleLines: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let textHeight = sizeThatFits(maxSize).height
        let lineHeight = font.lineHeight
        return Int(ceil(textHeight / lineHeight))
    }
}
