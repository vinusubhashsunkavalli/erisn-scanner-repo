//
//  UICollectionViewCell+Reuseid.swift
//  ProjectTemplate
//
//  Created by vamsi on 02/02/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

extension UICollectionViewCell{
    static var reuseid:String{
        return String(describing: self)
    }
}
