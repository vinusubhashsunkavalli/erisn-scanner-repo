//
//  UITextView+Placeholder.swift
//  Errand Junkie
//
//  Created by Ghazalah on 01/06/20.
//  Copyright © 2020 Ghazalah. All rights reserved.
//

import Foundation
import UIKit


//MARK:- Usage -
// func setupTextView(){
//        //To use Placeholder textview must set delegate to self programmatically
//
//        myTextView.delegate = self
////        myTextView.placeholder = "Enter Address"
////        myTextView.placeholderTextColor = .red
//        myTextView.placeholderFont = UIFont.systemFont(ofSize: 25, weight: .bold)
//    }
//
//}
//
//extension ViewController:UITextViewDelegate{
//
//    func textViewDidChange(_ textView: UITextView) {
//        print(textView.text ?? "")
//        if let placeholderLabel = textView.viewWithTag(100) as? UILabel {
//            placeholderLabel.isHidden = !textView.text.isEmpty
//        }
//    }
//}


extension UITextView: UITextViewDelegate {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    @IBInspectable var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    @IBInspectable var placeholderTextColor:UIColor?{
        get{
            return self.textColor ?? .black
        }set{
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.textColor = newValue
                placeholderLabel.sizeToFit()
            }
        }
    }
    
    @IBInspectable var placeholderFont:UIFont?{
        get{
            return self.font ?? UIFont.systemFont(ofSize: 15, weight: .regular)
        }set{
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.font = newValue
                placeholderLabel.sizeToFit()
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = !self.text.isEmpty
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = placeholderFont
        placeholderLabel.textColor = placeholderTextColor
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = !self.text.isEmpty
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
    
}
