//
//  UIImage.swift
//  ProjectTemplate
//
//  Created by vamsi on 24/03/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

extension UIImageView {
    
    var imageTintColor: UIColor {
        get {
            return self.tintColor
        }
        set {
            if let tempImage = image {
                image = tempImage.withRenderingMode(.alwaysTemplate)
            }
            tintColor = newValue
        }
    }
}
