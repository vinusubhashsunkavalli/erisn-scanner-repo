//
//  UIViewController+DatepickerExtension.swift
//  DateTimePicker
//
//  Created by Apple on 20/03/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

extension UITextField{
    
    func showDatePicker(_ minDate:Date = Date(),_ maxDate:Date = Date()){
        let datePicker = UIDatePicker()
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        datePicker.datePickerMode = .date
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.backgroundColor = .gray
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneDatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        datePicker.addTarget(self, action: #selector(setupTextField(_:)), for: .valueChanged)
        self.inputAccessoryView = toolbar
        self.inputView = datePicker
    }
    
    
    @objc func doneDatePicker(){
        if let picker = self.inputView as? UIDatePicker{
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM d, yyyy"
            self.text = formatter.string(from: picker.date)
        }
        self.resignFirstResponder()
        //            self.view.endEditing(true)
    }
    
    @objc func setupTextField(_ picker:UIDatePicker){
        if let picker = self.inputView as? UIDatePicker{
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM d, yyyy"
            self.text = formatter.string(from: picker.date)
//            self.resignFirstResponder()
        }
    }
    
    @objc func cancelDatePicker(){
        self.resignFirstResponder()
        //            self.view.endEditing(true)
    }
    
}
