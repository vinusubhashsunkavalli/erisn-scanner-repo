//
//  Storyboard+Extension.swift
//  BaseProject
//
//  Created by Tejinder Paul on 09/03/17.
//  Copyright © 2017 Debut Infotech. All rights reserved.
//


//let vc:ViewController2 =   UIStoryboard(storyboard: .Home, bundle: nil).controller()
//present(vc, animated: true, completion: nil)

import Foundation

import UIKit

protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

extension StoryboardIdentifiable where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}

enum Storyboard: String {
    case Main
    case HomeStory
    case HelpFAQ
        var filename: String {
        switch self {
        default: return rawValue
        }
    }
}

extension UIViewController: StoryboardIdentifiable { }

extension UIStoryboard {
    /// The uniform place where we state all the storyboard we have in our application


    // MARK: - Convenience Initializers

    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.filename, bundle: bundle)
    }

    // MARK: - View Controller Instantiation from Generics

    func controller<T: UIViewController>() -> T {
        guard let vc = self.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(T.storyboardIdentifier) ")
        }
        return vc
    }
}





