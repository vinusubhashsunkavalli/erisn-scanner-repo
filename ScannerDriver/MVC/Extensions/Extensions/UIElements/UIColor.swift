//
//  Global.swift
//  ProjectTemplate
//
//  Created by Apple on 17/03/20.
//  Copyright © 2020 vamsi. All rights reserved.
//
import UIKit

extension UIColor {
    static let yellow = UIColor("FFF05A")
    static let ayceColor = UIColor("FFF05A")
    static let paleGreen = UIColor("1BBC9C")
    static let paleBlue = UIColor("4CA7DC")
    static let primary = UIColor("F05B5D")
    static let bfpColor = UIColor("FF9000")
    static let ppColor = UIColor(red: 82/255.0, green: 187/255.0, blue: 156/255.0, alpha: 1.0)
    static let green = UIColor("4CD964")
    static let gray  = UIColor("DBDBDB")
}


extension UIColor {
    public static var app_theme: UIColor {
        return UIColor(named: "BaseColor")!
    }
    public static var homeTable_Color: UIColor {
        return UIColor(named: "BackGroundViewColor")!
    }
    public static var appThemeColor: UIColor {
        return UIColor(named: "AppThemeColor")!
    }
}

extension UIColor {
    convenience init(_ hex: String, alpha: CGFloat = 1.0) {
        var hexFormatted: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        if hexFormatted.hasPrefix("#") {
            hexFormatted = String(hexFormatted.dropFirst())
        }
        assert(hexFormatted.count == 6, "Invalid hex code used.")
        var rgbValue: UInt64 = 0
        Scanner(string: hexFormatted).scanHexInt64(&rgbValue)
        
        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: alpha)
    }
    
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
