//
//  UISearchBar.swift
//  ProjectTemplate
//
//  Created by vamsi on 02/02/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

extension UIViewController{
    func showAlert(withMessage message: String? = nil, title: String = "Alert", okayTitle: String = "Ok", cancelTitle: String? = nil,preferredStyle:UIAlertController.Style = .alert,doneBtnStyle:UIAlertAction.Style = .default , cancelCall: @escaping () -> () = { }, okCall: @escaping () -> () = { }) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        if let cancelTitle = cancelTitle {
            let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (_) in
                cancelCall()
            }
            
            alert.addAction(cancelAction)
        }
        let okayAction = UIAlertAction(title: okayTitle, style: doneBtnStyle) { (_) in
            okCall()
        }
        alert.addAction(okayAction)
        present(alert, animated: true)
    }
}

extension UIViewController{
    func showDateTimePickerInAlert(mode:UIDatePicker.Mode,date:Date? = Date(),minDate:Date? = nil,maxDate:Date? = nil,withMessage message: String? = nil, title: String = "Pick a Date", okayTitle: String = "Done", cancelTitle: String? = nil,preferredStyle:UIAlertController.Style = .alert,doneBtnStyle:UIAlertAction.Style = .default , cancelCall: @escaping () -> () = { }, okCall: @escaping () -> () = { },action:DatePickerViewController.Action?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        alert.addDatePicker(mode: mode, date: date, minimumDate: minDate, maximumDate: maxDate, action: action)
        if let cancelTitle = cancelTitle {
            let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (_) in
                cancelCall()
            }
            alert.addAction(cancelAction)
        }
        let okayAction = UIAlertAction(title: okayTitle, style: doneBtnStyle) { (_) in
            okCall()
        }
        alert.addAction(okayAction)
        present(alert, animated: true)
    }
}

extension UIViewController{
    func showPickerViewInAlert(values: PickerViewViewController.Values,  initialSelection: PickerViewViewController.Index? = nil,withMessage message: String? = nil, title: String = "Pick Something", okayTitle: String = "Ok", cancelTitle: String? = nil,preferredStyle:UIAlertController.Style = .alert,doneBtnStyle:UIAlertAction.Style = .default , cancelCall: @escaping () -> () = { }, okCall: @escaping () -> () = { }, action: PickerViewViewController.Action?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        alert.addPickerView(values: values,initialSelection: initialSelection, action: action)
        if let cancelTitle = cancelTitle {
            let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (_) in
                cancelCall()
            }
            
            alert.addAction(cancelAction)
        }
        let okayAction = UIAlertAction(title: okayTitle, style: doneBtnStyle) { (_) in
            okCall()
        }
        alert.addAction(okayAction)
        present(alert, animated: true)
    }
}

extension UIViewController{
    func showTextFieldsInAlert(noOfTextFields:Int = 1,configs:[TextField.Config],withMessage message: String? = nil, title: String = "Alert", okayTitle: String = "Ok", cancelTitle: String? = nil,preferredStyle:UIAlertController.Style = .alert,doneBtnStyle:UIAlertAction.Style = .default , cancelCall: @escaping () -> () = { }, okCall: @escaping () -> () = { }) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        
        if noOfTextFields == 1{
            alert.addOneTextField(configuration: configs.first)
        }else if noOfTextFields == 2{
            alert.addTwoTextFields(textFieldOne: configs.first, textFieldTwo: configs[1])
        }else{
            print("More than 2 is under construction")
        }
        
        if let cancelTitle = cancelTitle {
            let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (_) in
                cancelCall()
            }
            
            alert.addAction(cancelAction)
        }
        let okayAction = UIAlertAction(title: okayTitle, style: doneBtnStyle) { (_) in
            okCall()
        }
        alert.addAction(okayAction)
        present(alert, animated: true)
    }
}


//MARK : - Controller Extension
extension UIViewController {
    
    func add(asChildViewController vc: UIViewController, on view: UIView) {
        // Add Child View Controller
        addChild(vc)
        // Add Child View as Subview
        view.addSubview(vc.view)
        // Configure Child View
        vc.view.frame = view.bounds
        vc.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        vc.didMove(toParent: self)
    }
    
    func remove(asChildViewController vc: UIViewController) {
        // Notify Child View Controller
        vc.willMove(toParent: nil)
        
        // Remove Child View From Superview
        vc.view.removeFromSuperview()
        
        // Notify Child View Controller
        vc.removeFromParent()
    }
}

