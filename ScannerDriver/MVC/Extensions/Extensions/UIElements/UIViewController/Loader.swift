
//import UIKit
//import SVProgressHUD
//import MBProgressHUD
//import NVActivityIndicatorView
//
////MARK:- SVProgressHUD
//extension UIViewController{
//    func showLoader(message:String = "",color:UIColor = UIColor.lightGray.withAlphaComponent(0.4)){
//        SVProgressHUD.setDefaultMaskType(.custom)
//        SVProgressHUD.setDefaultStyle(.light)
//        SVProgressHUD.setDefaultAnimationType(.native)
//        SVProgressHUD.setBackgroundColor(color)
//        SVProgressHUD.setMinimumSize(.init(width: 95, height: 95))
//        SVProgressHUD.setBackgroundLayerColor(.clear)
//        if message.isNotEmpty{
//            SVProgressHUD.show(withStatus: message)
//        }else{
//            SVProgressHUD.show()
//        }
//    }
//    func showLoaderWithProgress(progress:Double) {
//        SVProgressHUD.setDefaultMaskType(.custom)
//        SVProgressHUD.showProgress(Float(progress), status: "Uploading...")
//    }
//    func hideLoader(){
//        SVProgressHUD.dismiss()
//    }
//}
////MARK:- MBProgressHUD
//extension UIViewController{
//    func showHUD(progressLabel:String = ""){
//        DispatchQueue.main.async{
//            let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
////            progressHUD.backgroundColor  = .red
//            progressHUD.label.text = progressLabel
//        }
//    }
//    
//    func dismissHUD() {
//        DispatchQueue.main.async{
//            MBProgressHUD.hide(for: self.view, animated: true)
//        }
//    }
//}
//
////MARK:- NVActivity IndicatorView
//extension UIViewController:NVActivityIndicatorViewable {}
//extension UIViewController{
//    //MARK: - Loader
//    func  showNVLoader(with message: String = "", color: UIColor = UIColor.black.withAlphaComponent(0.7)){
//        let indictorSize = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 50)
//        startAnimating(indictorSize.size, message: message,type: .circleStrokeSpin,color: color,backgroundColor: .clear)
//    }
//    
////    func onlyLoaderWithoutMessageLabel(color: UIColor = UIColor.black.withAlphaComponent(0.7)){
////        let activityContainer: UIView = UIView(frame: UIScreen.main.bounds)
////        let indictorSize = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 50)
////        let activityIndicatorView = NVActivityIndicatorView(frame: indictorSize, type: NVActivityIndicatorType.circleStrokeSpin, color: color, padding: 0)
////        activityContainer.tag = 601601
////        activityIndicatorView.center = activityContainer.center
////        activityIndicatorView.startAnimating()
////        activityContainer.addSubview(activityIndicatorView)
////        view.addSubview(activityContainer)
////    }
//    
//    func hideNVLoader(){
//        stopAnimating()
//        //         NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//        //        for item in view.subviews where item.tag == 601601 {
//        //            item.removeFromSuperview()
//        //        }
//    }
//    
//}
//
////MARK:-  Toast
//extension UIViewController{
//    func showToast(message : String,duration:Double = 5.0) {
//        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/7.5, y: self.view.frame.size.height-65, width: self.view.frame.size.width - 2*(self.view.frame.size.width/7.5), height: 40))
//        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.8)
//        toastLabel.textColor = UIColor.white
//        toastLabel.textAlignment = .center;
//        toastLabel.font = .systemFont(ofSize: 16.0)
//        toastLabel.text = message
//        toastLabel.alpha = 1.0
//        toastLabel.layer.cornerRadius = 20
//        toastLabel.clipsToBounds = true
//        // toastLabel.center = self.view.center
//        self.view.addSubview(toastLabel)
//        UIView.animate(withDuration: duration, delay: 0.1, options: .curveEaseOut, animations: {
//            toastLabel.alpha = 0.0
//        }, completion: {(isCompleted) in
//            toastLabel.removeFromSuperview()
//        })
//    }
//}
