//
//  Notification.swift
//  ProjectTemplate
//
//  Created by vamsi on 24/03/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let packageInfo = Notification.Name("packageInfo")
    static let lightBox = Notification.Name("LightBox")
}
