//
//  Global.swift
//  ProjectTemplate
//
//  Created by Apple on 17/03/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import UIKit

typealias callBack = (()->())?

struct Global {
    func jsonToData(json: Any) -> (response:Data?,error:Error?) {
          do {
              return (try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted),nil)
          } catch let myJSONError {
              return (nil,myJSONError)
          }
      }
    
    func setRootViewController(viewController: UIViewController) {
       let navController = UINavigationController(rootViewController: viewController)
       navController.isNavigationBarHidden = true
       if appDelegate.window == nil {
          //Create New Window here
          appDelegate.window = UIWindow.init(frame: UIScreen.main.bounds)
       }
       appDelegate.window?.rootViewController = navController
       appDelegate.window?.makeKeyAndVisible()
    }
}
