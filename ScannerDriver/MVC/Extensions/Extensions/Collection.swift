//
//  Collection.swift
//  ProjectTemplate
//
//  Created by vamsi on 02/02/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import Foundation

extension Collection{
    var isNotEmpty:Bool{
        return !isEmpty
    }
}

