//
//  Date.swift
//  ProjectTemplate
//
//  Created by vamsi on 18/03/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import Foundation

enum DateFormattersType:String {
    case display = "dd MMM yyyy"
    case api = "yyyy-MM-dd"
    case apiTimeZone2 = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    case apiTimeZone = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    case monthDateYear = "MMM d, yyyy"
    case monthYear = "MMMM yyyy"
    case monthDateTime = "MMM d, h:mm a"
    case monthSlashes = "MM/dd/yyyy"
    case full = "dd/MM/yyyy hh:mm a"
    case short = "dd/MM/yyyy"
    case month = "MMM"
    case day = "dd"
    case weekDay = "EEE"
    case time12Hours = "h:mm a"
    case time12HourSec = "h:mm:ss a"
    case time24HourSec = "HH:mm:ss"
    case time24Hours = "HH:mm"
}

extension Date {
    
    func toString(dateFormat:DateFormattersType) -> String{
      let dateformatter = DateFormatter.defaultDateFormatter()
      dateformatter.dateFormat = dateFormat.rawValue
      let string = dateformatter.string(from: self)
      return string
    }
    
    func dayOfTheWeek() -> String {
        let dateFormatter = DateFormatter.defaultDateFormatter()
        dateFormatter.dateFormat = "EEE"
        return dateFormatter.string(from: self)
    }
}


extension Date {
   var yesterday: Date {
        return Calendar.defaultCalendar().date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.defaultCalendar().date(byAdding: .day, value: 1, to: noon)!
    }
    func getAfterOrbefore(component:Calendar.Component,days:Int = 1,date:Date = Date().noon) -> Date{
        return Calendar.defaultCalendar().date(byAdding: component, value: days, to: date)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var day: Int {
        return Calendar.current.component(.day,  from: self)
    }
    
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
}

extension Date{
   var getStartAndEndDateOfTheMonth:[String]{
      var startDateOfTheMonth = ""
      var endDateOfTheMonth = ""
      let dateFormatter = DateFormatter.defaultDateFormatter()
      dateFormatter.dateFormat = "yyyy-MM-dd"
      let calendar = Calendar.current
      let components = calendar.dateComponents([.year,.month], from: self)
      if let startOfMonth = calendar.date(from: components){
         if let endOfMonth =  calendar.date(byAdding: DateComponents(month: 1, day: -1), to: startOfMonth){
          endDateOfTheMonth =  dateFormatter.string(from: endOfMonth)
         }
         startDateOfTheMonth = dateFormatter.string(from: startOfMonth)
      }
      return [startDateOfTheMonth,endDateOfTheMonth]
   }
}

extension Date {
    func timesAgoDisplay() -> String {
        let secondsAgo = Int(Date().timeIntervalSince(self))
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        
        if secondsAgo < minute {
            if secondsAgo <= 1{
                return "\(secondsAgo) second ago"
            }else{
                return "\(secondsAgo) seconds ago"
            }
        } else if secondsAgo < hour {
            if secondsAgo / minute <= 1{
                return "\(secondsAgo / minute) minute ago"
            }else{
                return "\(secondsAgo / minute) minutes ago"
            }
        } else if secondsAgo < day {
            if (secondsAgo / hour) > 1{
                return "\(secondsAgo / hour) hours ago"
            }
            return "\(secondsAgo / hour) hour ago"
            
        } else if secondsAgo < week {
            if (secondsAgo / day) > 1{
                return "\(secondsAgo / day) days ago"
            }
            return "\(secondsAgo / day) day ago"
        }
        return (secondsAgo/week) <= 1 ? "\(secondsAgo / week) week ago" : "\(secondsAgo / week) weeks ago"
    }
    
}


