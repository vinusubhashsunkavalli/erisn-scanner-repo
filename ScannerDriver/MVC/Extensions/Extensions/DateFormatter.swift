//
//  DateFormatter.swift
//  ProjectTemplate
//
//  Created by vamsi on 18/03/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import Foundation

extension Calendar {
    
    static func defaultCalendar() -> Calendar{
        return Calendar(identifier: .gregorian)
    }
    
}

extension DateFormatter {
    class func defaultDateFormatter(_ dateformatterType:DateFormattersType = .api) -> DateFormatter {
        let formatter = DateFormatter()
//        let gregorianCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        // formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        //formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.calendar = Calendar.defaultCalendar()
        formatter.timeZone = NSTimeZone.local
        formatter.locale = Locale.current
        formatter.dateFormat = dateformatterType.rawValue
        return formatter
    }
}
