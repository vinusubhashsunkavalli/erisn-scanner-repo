//
//  Thumbnailmage.swift
//  Exactech
//
//  Created by Quventix Solutions on 17/09/19.
//  Copyright © 2019 vamsi. All rights reserved.
//

import UIKit
import Photos


extension URL{
    var generateThumbnailImage:UIImage? {
        do {
            let asset = AVURLAsset(url: self, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
}


