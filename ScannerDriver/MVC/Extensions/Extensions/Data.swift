//
//  Data.swift
//  ProjectTemplate
//
//  Created by Apple on 17/03/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import Foundation

extension Data{
    var dataToJSON:(response:Any?,error:Error?) {
        do {
            return (try JSONSerialization.jsonObject(with: self, options: .mutableContainers),nil)
        } catch let myJSONError {
            return (nil,myJSONError)
        }
    }
}


  
