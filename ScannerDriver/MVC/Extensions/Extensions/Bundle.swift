//
//  Bundle.swift
//  ProjectTemplate
//
//  Created by vamsi on 21/03/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import Foundation

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    
    var appDisplayName: String? {
        return infoDictionary?["CFBundleName"] as? String
    }
    
    var bundleIdentifier: String? {
        return infoDictionary?["CFBundleIdentifier"] as? String
    }
    
}
