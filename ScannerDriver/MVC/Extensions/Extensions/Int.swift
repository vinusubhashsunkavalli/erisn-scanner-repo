//
//  Int.swift
//  Exactech
//
//  Created by Quventix Solutions on 18/09/19.
//  Copyright © 2019 vamsi. All rights reserved.
//

import Foundation

extension Int{
    var string:String{
        return "\(self)"
    }
}
extension Int{
    func timerFormatted() -> String {
        let seconds: Int = self % 60
        let minutes: Int = (self / 60) % 60
        let hours: Int = (self / 3600) % 60
        return String(format: "%02d:%02d:%02d",hours, minutes, seconds)
    }
}
