//
//  String+first_Last_Name.swift
//  ProjectTemplate
//
//  Created by vamsi on 23/05/21.
//  Copyright © 2021 vamsi. All rights reserved.
//

import Foundation

// EX:- Sean Allen = SA
//EX:- Sean = SE

extension String {
    
    var initials: String {
        
        let words = components(separatedBy: .whitespacesAndNewlines)
        
        //to identify letters
        let letters = CharacterSet.alphanumerics
        var firstChar : String = ""
        var secondChar : String = ""
        var firstCharFoundIndex : Int = -1
        var firstCharFound : Bool = false
        var secondCharFound : Bool = false
        
        for (index, item) in words.enumerated() {
            
            if item.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                continue
            }
            
            //browse through the rest of the word
            for (_, char) in item.unicodeScalars.enumerated() {
                
                //check if its a aplha
                if letters.contains(char) {
                    
                    if !firstCharFound {
                        firstChar = String(char)
                        firstCharFound = true
                        firstCharFoundIndex = index
                        
                    } else if !secondCharFound {
                        
                        secondChar = String(char)
                        if firstCharFoundIndex != index {
                            secondCharFound = true
                        }
                        
                        break
                    } else {
                        break
                    }
                }
            }
        }
        
        if firstChar.isEmpty && secondChar.isEmpty {
            firstChar = "?"
            secondChar = "?"
        }
        return (firstChar + secondChar).uppercased()
    }
}

extension String{
    var initial: String {
        var initals = String()
        let names = self.components(separatedBy: " ")
        for name in names {
            if let firstChar = name.first {
                initals.append(firstChar)
            }
        }
        return initals
    }
}
