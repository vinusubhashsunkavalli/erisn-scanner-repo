//
//  Thumbnail.swift
//  Exactech
//
//  Created by Quventix Solutions on 17/09/19.
//  Copyright © 2019 vamsi. All rights reserved.
//

import UIKit

extension String{
    var isVideoUrl:Bool{
        return contains(".mp4") || contains(".MP4") || contains(".MOV") || contains("Video") ? true : false
    }
}

extension String{
    var url:URL{
       return URL(string: self)!
    }
}

extension String{
    var int:Int{
        return Int(self) ?? 0
    }
}

extension String{
    var withoutHtmlTags: String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    static func getVideoId(youtubeURL: String) -> String {
        var youtubeId: String?
        let regex = try! NSRegularExpression(pattern: "\\?.*v=([^&]+)", options: .caseInsensitive)
        let match = regex.firstMatch(in: youtubeURL, options: [], range: NSRange(location: 0, length: youtubeURL.count))
        if let videoIDRange = match?.range(at: 1) {
            youtubeId = (youtubeURL as NSString).substring(with: videoIDRange)
        } else {
            //NO video URL
        }
        return youtubeId ?? ""
    }
}
