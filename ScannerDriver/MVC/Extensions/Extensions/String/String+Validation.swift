//
//  String+Validation.swift
//  HungrayHub
//
//  Created by Tejinder Paul Singh  on 25/04/18.
//  Copyright © 2018 HungryHub. All rights reserved.
//

import Foundation

extension String{
    var hasSpecialCharacters:Bool {
        do {
            let regex = try NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: .caseInsensitive)
            if let _ = regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, self.count)) {
                return true
            }
        } catch {
            debugPrint(error.localizedDescription)
            return false
        }
        return false
    }
}


extension String {

    var isNotEmpty:Bool{
        return !isEmpty
    }

    /// This will check whether a string is empty or not by remove all spaces
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }

    /// This will check wheher a string is a valid email or not.
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)

    }

    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSRange(location: 0, length: self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }

    func isValidInRange(minLength min: Int = 0, maxLength max: Int) -> Bool {
        if self.count >= min && self.count <= max {
            return true
        }
        return false
    }

    func replace(_ string: String, replacement: String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: .literal, range: nil)
    }

    var trim: String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }


    var trimmed: String {
        let components = self.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
    
    var isContainsWhitespace : Bool {
        return(self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }

//    var localized: String {
//        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
//
//    }

    var isValidZip: Bool {
        let postalcodeRegex = "^[0-9]{5}(-[0-9]{4})?|[0-9]{5}$"
        let pinPredicate = NSPredicate(format: "SELF MATCHES %@", postalcodeRegex)
        let bool = pinPredicate.evaluate(with: self) as Bool
        return bool
    }



}


extension Optional where Wrapped == String {
    var isBlank: Bool {
        guard let value = self else { return true }
//        value = value.trimmed
        return value.isEmpty
    }

    var isEmail: Bool {
        guard var email = self else { return false }
        email = email.trimmed
        return email.isEmail
    }

    var isPhoneNumber: Bool {
        guard var phoneNumber = self else { return false }
        phoneNumber = phoneNumber.trimmed
        return phoneNumber.isPhoneNumber
    }
    
    var trimmed: String {
        guard let str = self else {
            return ""
        }
        return str.trimmed
    }
    var addTrailingAndLeadingSpace: String {
        guard let value = self else { return " " }
        return "    " + value + "   "
    }
    var isContainsWhitespace : Bool {
        guard let value = self else { return false}
        return(value.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
}

extension NSAttributedString {
    internal convenience init?(html: String) {
        guard let data = html.data(using: String.Encoding.utf16, allowLossyConversion: false) else {
            // not sure which is more reliable: String.Encoding.utf16 or String.Encoding.unicode
            return nil
        }
        guard let attributedString = try? NSMutableAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) else {
            return nil
        }
        self.init(attributedString: attributedString)
    }
}



extension String {
    func matches(for regex: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: self, range: NSRange(self.startIndex..., in: self))
            return results.count > 0
        } catch {
            return false
        }
    }
//    var isvalidName: Bool {
//        return self.matches(for: Constants.Regex.name)
//    }
}
