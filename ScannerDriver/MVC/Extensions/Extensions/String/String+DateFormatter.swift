//
//  String+SubScript.swift
//  Liscio
//
//  Created by Tejinder Paul Singh  on 14/04/18.
//  Copyright © 2018 HungryHub. All rights reserved.
//

import Foundation

extension String{
    
//    Usage
//    let apiDate = "2018-09-11T09:53:25.000+0000"
//    print(apiDate.getFormatedDateString())
//    11 Sep 2018
    
//    let temp = "2018-09-11"
//    print(temp.getFormatedDateString(currentDateFormat: .api))
//    11 Sep 2018

    
    func getFormatedDateString(currentDateFormat:DateFormattersType = .apiTimeZone,requiredFormat:DateFormattersType = .display) -> String{
        let dateFormatter = DateFormatter.defaultDateFormatter()
        dateFormatter.dateFormat = currentDateFormat.rawValue
        let dateFromInputString = dateFormatter.date(from: self)
        dateFormatter.dateFormat = requiredFormat.rawValue // Here you can use any dateformate for output date
        if dateFromInputString != nil {
            return dateFormatter.string(from: dateFromInputString!)
        }
        else{
            debugPrint("could not convert date")
            return ""
        }
    }
}

extension String {
    
    func calculateTime(currentDateFormat:DateFormattersType = .apiTimeZone,requiredFormat:DateFormattersType = .display) -> String {
        let dateFormatter2 = DateFormatter.defaultDateFormatter()
        dateFormatter2.dateFormat = requiredFormat.rawValue
        let dateFormatter = DateFormatter.defaultDateFormatter()
        dateFormatter.dateFormat = currentDateFormat.rawValue
        let startDate = self
        guard let formatedStartDate = dateFormatter.date(from: startDate) else{return ""}
        let currentDate = Date()
        let components = Set<Calendar.Component>([.second, .minute, .hour, .day, .month, .year])
        let differenceOfDate = Calendar.current.dateComponents(components, from: formatedStartDate, to: currentDate)
        guard let years = differenceOfDate.year  else{return ""}
        guard let months = differenceOfDate.month  else{return ""}
        guard let days = differenceOfDate.day  else{return ""}
        guard let hours = differenceOfDate.hour  else{return ""}
        guard let minutes = differenceOfDate.minute  else{return ""}
        guard let seconds = differenceOfDate.second  else{return ""}
        
        if years > 0 {
            let date = dateFormatter2.string(from: formatedStartDate)
            return "\(date)"
        }else if months > 0 {
            let date = dateFormatter2.string(from: formatedStartDate)
            return "\(date)"
        }else if days > 0 {
            if days >= 7 {
                return formatedStartDate.timesAgoDisplay()
            }else{
                if days > 1{
                    return "\(days) days ago "
                }else{
                    return "\(days) day ago "
                }
            }
        }else if hours > 0 {
            return "\(hours) hours ago "
        }else if minutes > 0 {
            return "\(minutes) minutes ago "
        }else if seconds > 0 {
            return "\(seconds) seconds ago "
        }
        return "1 second ago"
    }
}
