
//
//  ReusableImagePickerController.swift
//  ReusableImagePicker
//
//  Created by vamsi on 15/10/19.
//  Copyright © 2019 vamsi. All rights reserved.
//

import UIKit

enum PickerAlertOptions {
    case camera
    case gallery
    case photoLibrabry
}

public protocol ImagePickerDelegate: class {
    func didSelect(image: UIImage)
}

class ReusableImagePickerController: NSObject {
    
    private let imagePickerController = UIImagePickerController()
    private weak var presentationController: UIViewController?
    weak var delegate: ImagePickerDelegate?
    
    var allowsEditing:Bool = true{
        didSet{
            imagePickerController.allowsEditing = allowsEditing
        }
    }
    
    var sourceType:UIImagePickerController.SourceType = .photoLibrary{
        didSet{
           imagePickerController.sourceType = sourceType
        }
    }
    
    init(controller:UIViewController){
        presentationController = controller
        super.init()
        self.imagePickerController.delegate = self
    }
    
    /// Without Alert
    func present(){
        presentationController?.present(imagePickerController, animated: true, completion: nil)
    }
    
    func showAlert(title:String = "" , message:String = "",alertOptions:[PickerAlertOptions] = [.gallery],style:UIAlertController.Style = .actionSheet){
        let alertController = UIAlertController(title: title == "" ? nil : title, message: message == "" ? nil : message, preferredStyle: style)
        alertOptions.forEach { (option) in
            switch option {
            case .camera:
                if let action = self.action(for: .camera, title: "Take photo") {
                    alertController.addAction(action)
                }
            case .photoLibrabry:
                if let action = self.action(for: .photoLibrary, title: "Photo library") {
                    alertController.addAction(action)
                }
            case .gallery:
                if let action = self.action(for: .savedPhotosAlbum, title: "Gallery") {
                    alertController.addAction(action)
                }
            }
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.presentationController?.present(alertController, animated: true)
    }
    
    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        //For simulator camera will not show
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.imagePickerController.sourceType = type
            self.present()
        }
    }
}

extension ReusableImagePickerController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var image = UIImage()
        if let img = info[.editedImage] as? UIImage{
            image = img
        }else if let img = info[.originalImage] as? UIImage{
            image = img
        }
        delegate?.didSelect(image: image)
        presentationController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        presentationController?.dismiss(animated: true, completion: nil)
    }
}
