//
//  String+Extension.swift
//  Meaty
//
//  Created by vinnu subhash on 24/04/20.
//  Copyright © 2020 ERSIN. All rights reserved.
//

import Foundation

extension String {
    
    var isValidIndianContact: Bool {
         let phoneNumberRegex = "^[6-9]\\d{9}$"
         let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
         let isValidPhone = phoneTest.evaluate(with: self)
         return isValidPhone
     }
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    var isValidName: Bool {
          let RegEx = "^\\w{3,18}$"
          let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
          return Test.evaluate(with: self)
       }
    
}
extension String {

   
}
