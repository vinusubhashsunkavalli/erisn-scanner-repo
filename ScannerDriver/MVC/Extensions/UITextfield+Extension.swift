//
//  UITextfield+Extension.swift
//  Meaty
//
//  Created by vinnu subhash on 02/06/20.
//  Copyright © 2020 ERSIN. All rights reserved.
//

import UIKit

extension UITextField{
    
    
    func leftImage(imageName : String){
      
        self.leftViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        let spacing : CGFloat = 10; // the amount of spacing to appear between image and title
       // self.imageInsets = UIEdgeInsetsMake(0, 0, 0, spacing);
        
        let image = UIImage(named: imageName)
        imageView.image = image
        imageView.contentMode = .scaleAspectFill
        self.leftView = imageView
        
        
    }
    
    func rightImage(imageName : String){
        
          self.rightViewMode = UITextField.ViewMode.always
          let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
          let image = UIImage(named: imageName)
          imageView.image = image
          self.rightView = imageView
          
          
      }
    
    
    
    func setInputViewDatePicker(target: Any, selector: Selector) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        // iOS 14 and above
        if #available(iOS 14, *) {// Added condition for iOS 14
          datePicker.preferredDatePickerStyle = .wheels
          datePicker.sizeToFit()
        }
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
    
    
    
}

