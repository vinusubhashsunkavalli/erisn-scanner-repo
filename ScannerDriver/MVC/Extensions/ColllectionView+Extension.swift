//
//  ColllectionView+Extension.swift
//  Meaty
//
//  Created by Apple on 19/03/20.
//  Copyright © 2020 ERSIN. All rights reserved.
//

import UIKit


extension UICollectionView{
    
    static func xibFile(colviewcell :UICollectionViewCell) {
        UICollectionView.init().register(UINib(nibName: "\(colviewcell)", bundle: nil), forCellWithReuseIdentifier: "\(colviewcell)")
    }
    
    
}
