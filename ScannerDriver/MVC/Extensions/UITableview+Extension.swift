//
//  UITableview+Extension.swift
//  DBSouqMain
//
//  Created by Apple on 29/11/19.
//  Copyright © 2019 Illyas Pasha. All rights reserved.
//

import UIKit

extension UITableView {
    
    func setEmptyMessage(_ message: String,fontColor : UIColor = .red) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = fontColor
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Roboto-Bold", size: 16)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
        // self.separatorStyle = .none
    }
}
