//
//  StoryboardConstant.swift
//  LMSErisn
//
//  Created by vinnu subhash on 12/01/21.
//  Copyright © 2021 ERSIN. All rights reserved.
//

import Foundation
import UIKit

//let KMainStoryBoard = UIStoryboard.init(name:"Main", bundle: nil)

let loginStoryboard = StoryBoard.LoginStory.get
let securityGuardStory = StoryBoard.SecurityGuardStory.get
let driverStoryboard = StoryBoard.Driver.get
let schoolStoryboard = StoryBoard.SchoolListStory.get


var appDelegate = UIApplication.shared.delegate as! AppDelegate

enum StoryBoard :String
{
    case LoginStory
    case SecurityGuardStory
    case Driver
    case SchoolListStory
  
    var get:UIStoryboard
    {
        let sb  = UIStoryboard.init(name: self.rawValue, bundle: nil)
        return  sb
    }
}
