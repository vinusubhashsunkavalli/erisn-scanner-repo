//
//  BackActionNavBarView.swift
//  StudentLMS
//
//  Created by vinnu subhash on 29/03/21.
//

import UIKit

class BackActionNavBarView: UIView {


    @IBOutlet var backActionNavview: UIView!
    
    var backClosure : (()->())!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
       
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("BackActionNavBarView", owner: self, options: nil)
        addSubview(backActionNavview)
        backActionNavview.frame = self.bounds
        backActionNavview.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        setup()
    }
    
    func setup(){
        
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        print("Back Nav Bar is callled")
        backClosure?()
        
    }
    

}
