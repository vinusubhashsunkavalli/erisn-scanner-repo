//
//  LogoNameBar.swift
//  StudentLMS
//
//  Created by vinnu subhash on 29/03/21.
//

import UIKit
import DropDown

class LogoNameBar: UIView {

    @IBOutlet var realNameView: UIView!
    @IBOutlet weak var logoImgView: UIImageView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var downBtnOut: UIButton!
    @IBOutlet weak var imageBtnOut: UIButton!
    
   
    
    var dropDownClosure : (()->())!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
        setup()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("LogoNameBar", owner: self, options: nil)
        addSubview(realNameView)
        realNameView.frame = self.bounds
        realNameView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        setup()
    }
    
    func setup(){
        
        imageBtnOut.layer.cornerRadius = imageBtnOut.frame.width/2
    
    }
    
    
    @IBAction func imageAction(_ sender: UIButton) {
        dropDownClosure?()
    }
    
    @IBAction func arrowDropAction(_ sender: UIButton) {
        dropDownClosure?()
    }
    
    
}
