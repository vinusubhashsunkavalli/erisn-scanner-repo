//
//  UserdefaulsConstants.swift
//  Meaty
//
//  Created by Apple on 19/03/20.
//  Copyright © 2020 ERSIN. All rights reserved.
//

import Foundation

//Mark: New push


struct  UserdefaultsConstants {
    
    
    
    static let isNepaliCalendar = true
    
    static let userType = "userType"
    
    static let walkThroughDef = "walkThroughDef"
    
    static let pushTokeID = "pushTokenID"
    static let tokenID = "tokenID"
    static let userID = "userID"
    static let phnNum = "phnNum"
    
    static let userName = "userName"
    
    static let profilePic = "profilePic"
    static let savedAddressStringDefault = "savedAddressStringDefault"
    static let branchIdDefault = "branchIdDefault"
    static let pincodeUserdefaults = "pincodeUserdefaults"
    static let areaUserdefaults = "areaUserdefaults"
    static let phnNumDefaults = "phnNumDefaults"
    
    
    static let userTypeDefaults = "userTypeDefaults"
    
    
    static let schoolLogoURL = "schoolLogoURL"
    static let baserURLConstant = "baserURLConstant"
    static let isSchoolSelected = "isSchoolSelected"
    static let schoolNameDefaults = "schoolNameDefaults"
       
}
