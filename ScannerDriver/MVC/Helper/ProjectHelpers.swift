//
//  ProjectHelpers.swift
//  DBNew
//
//  Created by Apple on 16/12/19.
//  Copyright © 2019 vinnu. All rights reserved.
//

import UIKit
import CoreData
import NVActivityIndicatorView

enum UserType{
    
    case scanner,driver
}

class ProjectHelpers {
    
    
    static func popController(vc : UIViewController){
        vc.navigationController?.popViewController(animated: true)
    }
    
    static func popTeacherController(vc : UIViewController){
        vc.navigationController?.popViewController(animated: true)
    }
 
    static func isDateInbetween(startDate : String,endDate : String)->Bool{
        
        let dateNow = Date()
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "dd-MM-yyyy" //Your date format
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+00:30") //Current time zone
       // dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        //according to date format your date string
//        guard let startDate = dateFormatter.date(from: "2021-01-28 08:30 AM") else {
//            fatalError()
//        }
//        guard let endDate = dateFormatter.date(from: "2021-01-28 11:30 AM") else {
//            fatalError()
//        }
//
//        guard let startDate = dateFormatter.date(from: "2021-01-28 08:30 AM") else {
//            fatalError()
//        }
//        guard let endDate = dateFormatter.date(from: "2021-01-28 11:30 AM") else {
//            fatalError()
//        }
        
        guard let startDate = dateFormatter.date(from: startDate) else {
            fatalError()
        }
        guard let endDate = dateFormatter.date(from: endDate) else {
            fatalError()
        }
        
        
        print(startDate)
        print("start date  is \(startDate)")
        print("Enddate is \(endDate)")
        print("Below one is present date")
        print(dateNow)
        let fallsBetween = (startDate ... endDate).contains(Date())
        print(fallsBetween)
        return fallsBetween
    }
    
    
    func dateform()->String{
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = df.string(from: date)
        print(dateString)
        return dateString
    }
    
    
    
   
    
    
    
    static func getToken()->String{
        let defaults = UserDefaults.standard
        if let token = defaults.value(forKey: UserdefaultsConstants.tokenID){
            
            print(token)
            
            let x = "\(token)"
            
            return x
        }else{
            return "0"
        }
    }
    

    
    
    static func userType()->String{
        
        let defaults = UserDefaults.standard
        if let user = defaults.value(forKey: UserdefaultsConstants.userTypeDefaults){
            if user as! String == "driver"{
                return "\(user)"
            }else{
                return "scanner"
            }
                    
    }
        else{
            return "driver"
        }

    }

    static func checkUserPresent()->Bool{
        
        let defaults = UserDefaults.standard
        if let _ = defaults.value(forKey: UserdefaultsConstants.userID){
            return true
        }else{
            return false
        }
        
    }
    
    static func getUser()->String{
        
        let defaults = UserDefaults.standard
        if let user = defaults.value(forKey: UserdefaultsConstants.userID){
            
            print(user)
            
            let x = "\(user)"
            
            return x
        }else{
            return "0"
        }
        
        
    }
    
    static func percentCalc(ofPrice: String,orginalPrice : String)->String{
        let off = Double(ofPrice)!/Double(orginalPrice)!
        let x = off*100
        let y = 100 - abs(Int32(x))
        return String(y)+"% OFF"
    }
    
    static func strikeThrough(titleStr : String) -> NSMutableAttributedString {
        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: titleStr)
        attributeString.addAttribute(NSAttributedString.Key.baselineOffset, value: 1, range: NSMakeRange(0, attributeString.length ))
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length  ))
        attributeString.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor.gray, range:  NSMakeRange(0, attributeString.length))
        return attributeString
    }
    
    //MARK:ShowAlert
    static func showAlert(for alert: String,vc : UIViewController) {
        let alertController = UIAlertController(title: "ALERT", message: alert, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    
    
    //MARK:Extra Methods
   
    
    
    //MARK: Loading
    
    let frame = CGRect(x: 0, y: 0, width: Theme.loadingWidth, height: Theme.loadingHeight)
    static let activityIndicatorView =  NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: Theme.loadingWidth, height: Theme.loadingHeight), type: Theme.loadingType, color: Theme.appPurpleColor, padding: nil)
    
    static func forLoading(vc : UIViewController){
        
        //activ = activityIndicatorView
        let vieww = vc.view
        activityIndicatorView.center =  vieww!.center
        vieww!.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
    }
    static func stopLoading(vc : UIViewController){
        activityIndicatorView.stopAnimating()
    }
    
//    static func cartBadge(){
//        
//        if let homeCartCou
//        
//    }
    
  
    
}
//MARK: School List
extension ProjectHelpers{
    
    
    
    static func getBaserURLString()->String{
        let defaults = UserDefaults.standard
        if let token = defaults.value(forKey: UserdefaultsConstants.baserURLConstant){
            
            print(token)
            
            let x = "\(token)"
            
            return x
        }else{
            return "0"
        }
    }
    
    static func isSchoolSelected()->Bool{
        let defaults = UserDefaults.standard
        if let token = defaults.value(forKey: UserdefaultsConstants.isSchoolSelected){
            
            print(token)
            
            return token as! Bool
        
        }else{
            return false
        }
    }
    
    
    
    static func getLogoString()->String{
        let defaults = UserDefaults.standard
        if let token = defaults.value(forKey: UserdefaultsConstants.schoolLogoURL){
            
            print(token)
            
            let x = "\(token)"
            
            return x
        }else{
            return "0"
        }
    }
    
    
    
}
