//
//  ProjectHelpers.swift
//  DBNew
//
//  Created by Apple on 16/12/19.
//  Copyright © 2019 vinnu. All rights reserved.
//

import UIKit
import CoreData
import NVActivityIndicatorView

class UtilityMethods {
    
    
  

    static func nullToNil(value : AnyObject?,stringValue : String) -> String {
        if value is NSNull {
            return stringValue
        } else {
            return value as! String
        }
    }
    
    
    
    static func checkUserPresent()->Bool{
        
        let defaults = UserDefaults.standard
        if let _ = defaults.value(forKey: UserdefaultsConstants.userID){
            return true
        }else{
            return false
        }
        
    }
    
    
    static func getUserName()->String{
        
        let defaults = UserDefaults.standard
        if let user = defaults.value(forKey: UserdefaultsConstants.userName){
            
            print(user)
            
            let x = "\(user)"
            
            return x
        }else{
            return " "
        }

    }
    
    static func getUserFirstName()->String{
        
        let defaults = UserDefaults.standard
        if let user = defaults.value(forKey: UserdefaultsConstants.userName){
            
            print(user)
            
            let x = "\(user)"
            
            if x != " " || x != ""{
                let re = x.components(separatedBy: " ")
                return re[0]
            }else{
                return " "
            }
        }else{
            return " "
        }

    }
    
    
    
    
    static func getUserNameInitial()->String{
        
        let defaults = UserDefaults.standard
        if let user = defaults.value(forKey: UserdefaultsConstants.userName) as? String{
            
            
            let sq = Array(user)[0]
            
            let index = 0
            
            let firstCharacter = user[String.Index.init(utf16Offset: index, in: user)]
            
            print(user)
            
            let x = "\(firstCharacter)"
            
            return x
        }else{
            return " "
        }

    }
    
    
    
    
    static func getUser()->String{
        
        let defaults = UserDefaults.standard
        if let user = defaults.value(forKey: UserdefaultsConstants.userID){
            
            print(user)
            
            let x = "\(user)"
            
            return x
        }else{
            return "0"
        }
        
        
    }
    
    static func percentCalc(ofPrice: String,orginalPrice : String)->String{
        let off = Double(ofPrice)!/Double(orginalPrice)!
        let x = off*100
        let y = 100 - abs(Int32(x))
        return String(y)+"% OFF"
    }
    
    static func strikeThrough(titleStr : String) -> NSMutableAttributedString {
        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: titleStr)
        attributeString.addAttribute(NSAttributedString.Key.baselineOffset, value: 1, range: NSMakeRange(0, attributeString.length ))
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length  ))
        attributeString.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor.gray, range:  NSMakeRange(0, attributeString.length))
        return attributeString
    }
    
    //MARK:ShowAlert
    static func showAlert(for alert: String,vc : UIViewController) {
        let alertController = UIAlertController(title: "ALERT", message: alert, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        vc.present(alertController, animated: true, completion: nil)
    }

    
    
    //MARK: Loading
//
//    let frame = CGRect(x: 0, y: 0, width: Themes.loadingWidth, height: Themes.loadingHeight)
//    static let activityIndicatorView =  NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: Themes.loadingWidth, height: Themes.loadingHeight), type: Themes.loadingType, color: Themes.redTheme, padding: nil)
//
//    static func forLoading(vc : UIViewController){
//
//        //activ = activityIndicatorView
//        let vieww = vc.view
//        activityIndicatorView.center =  vieww!.center
//        vieww!.addSubview(activityIndicatorView)
//        activityIndicatorView.startAnimating()
//
//    }
//    static func stopLoading(vc : UIViewController){
//        activityIndicatorView.stopAnimating()
//    }
    
}
