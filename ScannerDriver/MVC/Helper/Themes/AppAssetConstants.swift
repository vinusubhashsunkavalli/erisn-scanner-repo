//
//  AppAssetConstants.swift
//  StudentLMS
//
//  Created by vinnu subhash on 20/03/21.
//

import Foundation
import UIKit



let SblueTick = UIImage(named: "SblueTick")
let SgreyTick = UIImage(named: "SgreyTick")


struct AppAssetConstant {
    
//    AppAssetConstant.KtestExamIcon,AppAssetConstant.KMyGradesIcon,AppAssetConstant.KBellIcon,AppAssetConstant.KLogout
    
    
    
    
    
    static let KolorBlue = UIColor(named: "AppBlue")
    
    
    static let KonstantAppIcon = UIImage(named:"BrandLogo")
    
    //MARK: MoreVC
    static let KBellIcon = UIImage(named:"bell")
    static let KtestExamIcon = UIImage(named:"testExam")
    static let KMyGradesIcon = UIImage(named:"myGrades")
 // static let KNotificationIcon = UIImage(named:"ruler")
    static let KSettings = UIImage(named:"settings")
    static let KLogout = UIImage(named:"logout")
    
    static let KonstantRefreshIcon = UIImage(named:"refreshIcon")
  
//
//    static let KonstantAppIcon = UIImage(named:"diagramExam")

}
