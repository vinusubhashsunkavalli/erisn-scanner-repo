//
//  Themes.swift
//  Meaty
//
//  Created by vinnu subhash on 17/04/20.
//  Copyright © 2020 ERSIN. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class Theme {
    //MARK:Colors
    static let appRedColor = UIColor(named: "AppRed")
    static let appLightGrayColor = UIColor(named: "AppLightGray")
    static let appPurpleColor = UIColor(named:"AppPurple")
    
    
    //MARK:Fonts
    static let appMediumFont = "Roboto-Medium"
    static let appLightFont = "Roboto-Light"
    static let appRegularFont = "Roboto-Regular"
    static let appBoldFont = "Roboto-Bold"
    static let appSemiBoldFont = "Khula-SemiBold"
    static let headingFontSize  = 20
    static let subHeadingFontSize  = 17
    static let smallFontSize  = 15
    
    
    //MARK:Images
    
    //MARK:Driver Images
    static let busIcon = UIImage(named: "busIcon")
    static let tripsIcon = UIImage(named: "tripsIcon")
   

    //MARK: Scanner Images
    static let qrIcon = UIImage(named: "qrIcon")
    static let idIcon = UIImage(named: "idIcon")
  
  
    
    //MARK:Scanner Images
    
       static let loadingType = NVActivityIndicatorType.ballRotateChase
       static let loadingWidth = 60
       static let loadingHeight = 60
    
    
}
extension UIFont{
    
//    public enum FontName: String {
//          case appHeadingFont = Theme.
//           case semiboldItalic = "-SemiboldItalic"
//           case semibold = "-Semibold"
//           case regular = ""
//           case lightItalic = "Light-Italic"
//           case light = "-Light"
//           case italic = "-Italic"
//           case extraBold = "-Extrabold"
//           case boldItalic = "-BoldItalic"
//           case bold = "-Bold"
//       }
      
    static func fontMediumFunc(size : Int? = Theme.headingFontSize )->UIFont{
        return UIFont(name: Theme.appMediumFont, size: CGFloat(size!))!
    }
    static func fontLightFunc(size : Int? = 17 )->UIFont{
        return UIFont(name: Theme.appLightFont, size: CGFloat(size!))!
    }
    static func fontRegularFunc(size : Int? = 17 )->UIFont{
        return UIFont(name: Theme.appRegularFont, size: CGFloat(size!))!
    }
    static func fontBoldFunc(size : Int? = 17 )->UIFont{
        return UIFont(name: Theme.appBoldFont, size: CGFloat(size!))!
    }
 
}
