//
//  GaneshNetworkManager.swift
//  LMSProduct
//
//  Created by VinuSubhash Sunkavalli on 31/12/21.
//

import UIKit
import Alamofire




class JSONManager: NSObject {
    
    
    typealias RvCompletionHandler = ((_ result: Any?,_ Error: Error?) -> Void)?
    
    static let shared = JSONManager()
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

extension JSONManager {
    
    fileprivate func printAPI_Before(strURL:String = "", parameters:[String:Any] = [:], headers: HTTPHeaders = [:])
    {
        var str = "\(parameters)"
        str = str.replacingOccurrences(of: " = ", with: ":")
        str = str.replacingOccurrences(of: "\"", with: "")
        str = str.replacingOccurrences(of: ";", with: "")
        
        print("APi - \(strURL)\nParameters - \(str)\nHeaders - \(headers)")
    }
    
    fileprivate func printAPI_After(response :AFDataResponse<Any>)
    {
        if let value = response.value{
//            print("result.value: \((value as! [String:Any])["message"])")
//            print("result.value: \(value)")
//            if let response = (value as! [String:Any])["message"]{
//            }
            // result of response serialization
            if let dict = value as? [String:Any]{
                print(dict.json)
            }
        }
        if let error = response.error
        {
            print("result.error: \(error)") // result of response serialization
        }
    }
    
    
    fileprivate func hitApi(_ apiUrl: String = "",
                            type: HTTPMethod = .get,
                            parameter: [String:Any] = [:],
                            completionHandler : RvCompletionHandler
        ) {
        var strUrl : String = APIs.baseURL
        if apiUrl.contains("http") {
            strUrl = ""
        }
        if !strUrl.isEmpty && !apiUrl.isEmpty {
            strUrl.append("/")
        }
        strUrl.append(apiUrl)
        
        var headers : HTTPHeaders = [:]
        let token = ProjectHelpers.getToken()
        if !token.isEmpty {
            headers["x-api-key"] = token
        }
        
        JSONManager.shared.printAPI_Before(strURL: strUrl, parameters: parameter, headers: headers)
        let api = AF.request(strUrl, method: type, parameters: parameter, encoding: JSONEncoding.default, headers: headers)
        api.responseJSON { (responce) in
            JSONManager.shared.printAPI_After(response: responce)
            if let JSON = responce.data, JSON.count > 0 {
                completionHandler!(JSON, nil)
            }
            else if let Error = responce.error {
                completionHandler!(nil,Error as Error )
            }
            else {
                completionHandler!(nil , NSError(domain: "error", code: 117, userInfo: nil))
            }
        }
    }
    
    fileprivate func upLoadApi(_ apiUrl: String = "",
                               uploadObjects: [MultipartObject]?,
                               type: HTTPMethod = .post,
                               parameter : [String: Any] = [:],
                               completionHandler: RvCompletionHandler
        ) {
        var strUrl : String = APIs.baseURL
        if apiUrl.contains("http") {
            strUrl = ""
        }
        if !strUrl.isEmpty && !apiUrl.isEmpty {
            strUrl.append("/")
        }
        strUrl.append(apiUrl)

        var headers : HTTPHeaders = [:]
        let token = ProjectHelpers.getToken()
        if !token.isEmpty {
            headers["x-api-key"] = token
        }
        JSONManager.shared.printAPI_Before(strURL: strUrl, parameters: parameter, headers: headers)
        
      AF.upload(multipartFormData: { multipartFormData in
//             for (key, value) in parameter {
//                if let d = value as? [String : Any] {
//                  do {
//                    let data = try JSONSerialization.data(withJSONObject: d, options: .prettyPrinted)
//                    multipartFormData.append(data, withName: key)
//                  } catch {
//
//                  }
//                } else {
//                  multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
//                }
//             }
        
        for (key, value) in parameter {
            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
        }
             
             for data in uploadObjects! {
                 multipartFormData.append(data.dataObj, withName: data.strName, fileName: data.strFileName, mimeType: data.strMimeType)
             }
      }, to: strUrl, method: type, headers: headers)
        .responseJSON { response in
            debugPrint(response)
            
            JSONManager.shared.printAPI_After(response: response)
            
            if let JSON = response.data, JSON.count > 0 {
                completionHandler!(JSON, nil)
            }
            else if let Error = response.error {
                completionHandler!(nil,Error as Error )
            }
            else {
                completionHandler!(nil , NSError(domain: "error", code: 117, userInfo: nil))
            }
        }
    }
}

extension JSONManager {
 
    class func getAPI <T: Decodable>(_ apiUrl: String = "", parameters : [String:Any] = [:], Vc: UIViewController, showLoader : Bool = true, completionHandler : @escaping (T)->()) {
        if JSONManager.isConnectedToInternet {
            if showLoader{
               //  Constant.Loader.Show(view: Vc.view)
               // Indicator.shared().start()
            }
            var strUrl : String = APIs.baseURL
            if apiUrl.contains("http") {
                         strUrl = ""
                     }
                     if !strUrl.isEmpty && !apiUrl.isEmpty {
                         strUrl.append("/")
                     }
            strUrl.append(apiUrl)
            var headers : HTTPHeaders = [:] // ["Content-Type" : "application/x-www-form-urlencoded"]
            let token = ProjectHelpers.getToken()
            if !token.isEmpty {
                headers["x-api-key"] = token
            }
            JSONManager.shared.printAPI_Before(strURL: strUrl, parameters: parameters, headers : headers)
            let api = AF.request(strUrl, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            
            api.responseJSON { (responce) in
               // Constant.Loader.Hide()
              //  Indicator.shared().stop()
                JSONManager.shared.printAPI_After(response: responce)
                if responce.response?.statusCode == 200 || responce.response?.statusCode == 201{
                    if let JSON = responce.data, JSON.count > 0 {
                        do {
                            let obj = try JSONDecoder().decode(T.self, from: JSON)
                            completionHandler(obj)
                        }
                        catch let jsonError {
                            print("failed to download data", jsonError)
                        //    Vc.showAlert(message: "Session expired", strtitle: "Error")
                            print("failed to download data", jsonError)
                          //  checkJson(true)
                        }
                    }
                }
                else if responce.response?.statusCode == 401{
                    Vc.showAlert(message: "Session expired", strtitle: Constant.KAlert) { (_) in
                        let storybord = UIStoryboard(name: "Main", bundle: nil)
                        var frontView : UINavigationController! = storybord.instantiateViewController(withIdentifier: "MainNav") as? UINavigationController
                        let firstView : UIViewController! = storybord.instantiateViewController(withIdentifier: "LoginVC")
                        frontView = UINavigationController(rootViewController: firstView)
                        frontView.navigationBar.isHidden = true
                        UserDefaults.standard.removeObject(forKey: "userData")
                        UIApplication.shared.keyWindow?.rootViewController = frontView
                    }
                }
//                if let JSON = responce.data, JSON.count > 0 {
//                    do {
//                        let obj = try JSONDecoder().decode(T.self, from: JSON)
//                        completionHandler(obj)
//                    }
//                    catch let jsonError {
//                        print("failed to download data", jsonError)
//                    }
//                }
                else if let Error = responce.error {
                    Vc.showAlert(message: Error.localizedDescription, strtitle: "Error")
                }
                else {
                }
            }
        }
        else {
            Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
        }
    }
    
    class func getapi(_ apiUrl: String = "", parameters : [String:Any] = [:], Vc: UIViewController, showLoader : Bool = true, completionHandler : @escaping ([String:Any])->()) {
        if JSONManager.isConnectedToInternet {
            if showLoader {
             //   Constant.Loader.Show(view: Vc.view)
              //  Indicator.shared().start()//
            }
            var strUrl : String = APIs.baseURL
            
            strUrl.append(apiUrl)
            var headers : HTTPHeaders = [:] //["Content-Type" : "application/x-www-form-urlencoded"]
            let token = Constant.token
            if !token.isEmpty {
                headers["Authorization"] = token
            }
            JSONManager.shared.printAPI_Before(strURL: strUrl, parameters: parameters, headers : headers)
            let api = AF.request(strUrl, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            
            api.responseJSON { (responce) in
               // Constant.Loader.Hide()
               //  Indicator.shared().stop()
                JSONManager.shared.printAPI_After(response: responce)
                if let JSON = responce.data, JSON.count > 0 {
                    do {
                        let obj = try JSONSerialization.jsonObject(with: JSON , options: .init(rawValue: 0))
                        completionHandler(obj as! [String:Any])
                    }
                    catch let jsonError {
                        print("failed to download data", jsonError)
                    }
                }
                else if let Error = responce.error {
                    Vc.showAlert(message: Error.localizedDescription, strtitle: "Error")
                    
                }
                    
                else {
                }
            }
            
        }
        else {
            Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
        }
    }
    
    class func postapi(_ apiUrl: String = "", parameters : [String:Any] = [:], Vc: UIViewController, showLoader : Bool = true, completionHandler : @escaping ([String:Any])->()) {
        if JSONManager.isConnectedToInternet {
                   //    Constant.Loader.Show(view: Vc.view)
         //   Indicator.shared().start()
            var strUrl : String = APIs.baseURL
            
            strUrl.append(apiUrl)
            var headers : HTTPHeaders = [ : ]
            let token = ProjectHelpers.getToken()
            if !token.isEmpty {
                headers["x-api-key"] = token
            }
            JSONManager.shared.printAPI_Before(strURL: strUrl, parameters: parameters, headers : headers)
            let api = AF.request(strUrl, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            
            api.responseJSON { (responce) in
            //    Constant.Loader.Hide()
              //  Indicator.shared().stop()
                JSONManager.shared.printAPI_After(response: responce)
                if let JSON = responce.data, JSON.count > 0 {
                    do {
                        let obj = try JSONSerialization.jsonObject(with: JSON , options: .init(rawValue: 0))
                        completionHandler(obj as! [String:Any])
                    }
                    catch let jsonError {
                        print("failed to download data", jsonError)
                    }
                }
                else if let Error = responce.error {
                    Vc.showAlert(message: Error.localizedDescription, strtitle: "Error")
                    
                }
                    
                else {
                }
            }
        }
        else {
            Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
        }
    }
    
    
    class func hitpostAPI<T : Decodable>(with api : String,parameter : [String : Any],Vc: UIViewController,showloader : Bool = true, type : HTTPMethod = .post , completionHandler : @escaping (T)-> Void) {
           let apiUrl = api
           if JSONManager.isConnectedToInternet {
               if showloader {
                 //  Constant.Loader.Show(view: Vc.view)
             //   Indicator.shared().start()
               }
               JSONManager.shared.hitApi(apiUrl, type: type, parameter: parameter) { (result, error) in
                    //   Constant.Loader.Hide()
               // Indicator.shared().stop()
                   guard error == nil else {
                       //Vc.showAlert(message: (error?.localizedDescription)!, strtitle: "Error")
                       return}
                   guard let data = result else {return}
                   do {
                       let obj = try JSONDecoder().decode(T.self, from: data as! Data)
                       completionHandler(obj)
                   }
                   catch let jsonError  {
                       print("failed to download data", jsonError)
                   }
               }
           }
           else {
               Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
           }
       }
    
    class func hitApiSimple(with api : String,parameter : [String : Any],Vc: UIViewController,showloader : Bool = true, type : HTTPMethod = .post , completionHandler : @escaping ([String : Any]?)-> Void) {
        let apiUrl = api
        if JSONManager.isConnectedToInternet {
            if showloader {
              //  Indicator.shared().start()
            }
            JSONManager.shared.hitApi(apiUrl, type: type, parameter: parameter) { (result, error) in
                    //Indicator.shared().stop()
                guard error == nil else {
                    //Vc.showAlert(message: (error?.localizedDescription)!, strtitle: "Error")
                    return}
                guard let data = result else {return}
                do {
                    let obj = try? JSONSerialization.jsonObject(with: data as! Data, options: [])
                    completionHandler(obj as? [String : Any])
                }catch let jsonError  {
                    print("failed to download data", jsonError)
                }
            }
        }
        else {
            Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
        }

    }
    
        class func postAPI<T : Decodable>(_ apiUrl: String = "", parameters : [String:Any] = [:], type : HTTPMethod = .post ,Vc: UIViewController , completionHandler : @escaping (T) -> Void) {
//        let apiUrl = api.route
//        let parameter = api.parameter
        if JSONManager.isConnectedToInternet {
           // Constant.Loader.Show(view: Vc.view)
            //Indicator.shared().start()
               JSONManager.shared.hitApi(apiUrl, type: type, parameter: parameters) { (result, error) in
                 //      Constant.Loader.Hide()
                //Indicator.shared().stop()
                guard error == nil else {
                    Vc.showAlert(message: (error?.localizedDescription)!, strtitle: "Error")
                                       return}
                guard let data = result else {return}
                do {
                    let obj = try JSONDecoder().decode(T.self, from: data as! Data)
                    completionHandler(obj)
                }
                catch let jsonError  {
                    print("failed to download data", jsonError)
                     
                }
            }
        }
        else {
            Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
        }
    }
    
    class func deleteAPI<T : Decodable>(_ apiUrl: String = "", parameters : [String:Any] = [:], type : HTTPMethod = .delete ,Vc: UIViewController , completionHandler : @escaping (T) -> Void) {
            if JSONManager.isConnectedToInternet {
              //  Constant.Loader.Show(view: Vc.view)
                // Indicator.shared().start()
                   JSONManager.shared.hitApi(apiUrl, type: type, parameter: parameters) { (result, error) in
                       //    Constant.Loader.Hide()
                  //  Indicator.shared().stop()
                    guard error == nil else {
                        Vc.showAlert(message: (error?.localizedDescription)!, strtitle: "Error")
                                           return}
                    guard let data = result else {return}
                    do {
                        let obj = try JSONDecoder().decode(T.self, from: data as! Data)
                        completionHandler(obj)
                    }
                    catch let jsonError  {
                        print("failed to download data", jsonError)
                    }
                }
            }
            else {
                Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
            }
        }
    
    class func patchApi<T : Decodable>(_ apiUrl: String = "", parameters : [String:Any] = [:], type : HTTPMethod = .patch ,Vc: UIViewController , completionHandler : @escaping (T) -> Void) {
    //        let apiUrl = api.route
    //        let parameter = api.parameter
            if JSONManager.isConnectedToInternet {
              //  Constant.Loader.Show(view: Vc.view)
               // Indicator.shared().start()
                   JSONManager.shared.hitApi(apiUrl, type: type, parameter: parameters) { (result, error) in
                      //    Constant.Loader.Hide()
                  //  Indicator.shared().stop()
                    guard error == nil else {
                        Vc.showAlert(message: (error?.localizedDescription)!, strtitle: "Error")
                                           return}
                    guard let data = result else {return}
                    do {
                        let obj = try JSONDecoder().decode(T.self, from: data as! Data)
                        completionHandler(obj)
                    }
                    catch let jsonError  {
                        print("failed to download data", jsonError)
                         
                    }
                }
            }
            else {
                Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
            }
        }
    
    class func putAPI<T : Decodable>(_ apiUrl: String = "", parameters : [String:Any] = [:],Vc: UIViewController , completionHandler : @escaping (T)-> Void) {
        if JSONManager.isConnectedToInternet {
            JSONManager.shared.hitApi(apiUrl, type: .put, parameter: parameters) { (result, error) in
                guard error == nil else {
                    Vc.showAlert(message: (error?.localizedDescription)!, strtitle: "Error")
                    return}
                guard let data = result else {return}
                do {
                    let obj = try JSONDecoder().decode(T.self, from: data as! Data)
                    completionHandler(obj)
                }
                catch let jsonError  {
                    print("failed to download data", jsonError)
                }
            }
        }
        else {
            Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
        }
    }
    
    
    class func postApiWithImages<T : Decodable >(_ apiUrl: String = "",type: HTTPMethod, images: [UIImage], imageName: String,Vc: UIViewController, parameters : [String: Any] = [:],isAnimating : Bool = true, completion : @escaping (T) -> Void) {
        if JSONManager.isConnectedToInternet {
            if isAnimating {
          // Constant.Loader.Show(view: Vc.view)
              //  Indicator.shared().start()
            }
        //    guard let data: Data = image.jpegData(compressionQuality: 0.5)else{return}//UIImageJPEGRepresentation(image, 0.5)!
          
            let uploadimages : [MultipartObject] = {
                var objdata = [MultipartObject]()
                
                for image in images{
                    let data: Data = image.jpegData(compressionQuality: 0.5) ?? Data()
                    let name = Int(Date().timeIntervalSince1970)
                    let uploadData = MultipartObject(data: data, name: imageName, fileName: "\(name).jpg", mimeType: "image/jpg")
                    objdata.append(uploadData)
                }
                return objdata
            }()
            
            JSONManager.shared.upLoadApi(apiUrl,uploadObjects: uploadimages, type: type, parameter: parameters) { (result, error) in
             //   Constant.Loader.Hide()
               // Indicator.shared().stop()
                guard error == nil else {
                    Vc.showAlert(message: (error?.localizedDescription)!, strtitle: "Error")
                    return }
                guard let data = result else {return}
                do {
                    let obj = try JSONDecoder().decode(T.self, from: data as! Data)
                    completion(obj)
                }
                catch let jsonError {
                    print("failed to dowload data", jsonError)
                    Vc.showAlert(message: "Error", strtitle: jsonError.localizedDescription)
                }
            }
        }
        else {
            Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
        }
    }
    
    class func postApiWithDocumentImage<T : Decodable >(_ apiUrl: String = "", image1: UIImage, imageName1: String, image2: UIImage, imageName2: String,Vc: UIViewController, parameters : [String: Any] = [:],isAnimating : Bool = true, completion : @escaping (T) -> Void) {
        if JSONManager.isConnectedToInternet {
            if isAnimating {
          //  SVProgressHUD.show()
            //    Indicator.shared().start()
            }
            let data: Data = image1.jpegData(compressionQuality: 0.5)!//UIImageJPEGRepresentation(image, 0.5)!
            let name = Int(Date().timeIntervalSince1970)
//            let data1: Data = image2.jpegData(compressionQuality: 0.5)!//UIImageJPEGRepresentation(image, 0.5)!
//            let name1 = Int(Date().timeIntervalSince1970)
            let uploadData = MultipartObject(data: data, name: imageName1, fileName: "\(name).jpg", mimeType: "image/jpg")
            JSONManager.shared.upLoadApi(apiUrl, uploadObjects: [uploadData], parameter: parameters) { (result, error) in
             //   SVProgressHUD.dismiss()
               // Indicator.shared().stop()
                guard error == nil else {
                    Vc.showAlert(message: (error?.localizedDescription)!, strtitle: "Error")
                    return }
                guard let data = result else {return}
                do {
                    let obj = try JSONDecoder().decode(T.self, from: data as! Data)
                    completion(obj)
                }
                catch let jsonError {
                    print("failed to dowload data", jsonError)
                    Vc.showAlert(message: "Error", strtitle: jsonError.localizedDescription)
                }
            }
        }
        else {
            Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
        }
    }
    
    
    class func postApiWithImage<T : Decodable >(_ apiUrl: String = "",type: HTTPMethod, image: UIImage, imageName: String,Vc: UIViewController, parameters : [String: Any] = [:],isAnimating : Bool = true, completion : @escaping (T) -> Void) {
        if JSONManager.isConnectedToInternet {
            if isAnimating {
          //  Constant.Loader.Show(view: Vc.view)
              //  Indicator.shared().start()
            }
            guard let data: Data = image.jpegData(compressionQuality: 0.5)else{return}//UIImageJPEGRepresentation(image, 0.5)!
           
            let name = Int(Date().timeIntervalSince1970)
            let uploadData = MultipartObject(data: data, name: imageName, fileName: "\(name).jpg", mimeType: "image/jpg")
            
            JSONManager.shared.upLoadApi(apiUrl,uploadObjects: [uploadData], type: type, parameter: parameters) { (result, error) in
//                 Constant.Loader.Hide()
                // Indicator.shared().stop()
                guard error == nil else {
                    Vc.showAlert(message: (error?.localizedDescription)!, strtitle: "Error")
                    return }
                guard let data = result else {return}
                do {
                    let obj = try JSONDecoder().decode(T.self, from: data as! Data)
                    completion(obj)
                }
                catch let jsonError {
                    print("failed to dowload data", jsonError)
                    Vc.showAlert(message: "Error", strtitle: jsonError.localizedDescription)
                }
            }
        }
        else {
            Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
        }
    }
    
    func postGifImage <T : Decodable>(_ apiUrl: String = "", GifData: Data, imageName: String,Vc: UIViewController, parameters : [String: Any] = [:], completion : @escaping(T)->Void) {
        if JSONManager.isConnectedToInternet {
            let name = Int(Date().timeIntervalSince1970)
            let uploadData = MultipartObject(data: GifData, name: imageName, fileName: "\(name).gif", mimeType: "image/gif")
            JSONManager.shared.upLoadApi(apiUrl, uploadObjects: [uploadData], parameter: parameters) { (result, error) in
                
                guard error == nil else {
                    Vc.showAlert(message: (error?.localizedDescription)!, strtitle: "Error")
                    return }
                guard let data = result else {return}
                do {
                    let obj = try JSONDecoder().decode(T.self, from: data as! Data)
                    completion(obj)
                }
                catch let jsonError {
                    print("failed to dowload data", jsonError)
                    Vc.showAlert(message: "Error", strtitle: jsonError as! String)
                }
            }
        }
        else {
            Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
        }
    }
    
    class func postApiWithMultipleImage<T : Decodable >(_ apiUrl: String = "",type: HTTPMethod, imageArr: [UIImage], imageName: String,Vc: UIViewController, parameters : [String: Any] = [:],isAnimating : Bool = true, completion : @escaping (T) -> Void) {
        if JSONManager.isConnectedToInternet {
            if isAnimating {
          //  Constant.Loader.Show(view: Vc.view)
                Indicator.shared().start()
            }
            var imageData = [MultipartObject]()
            if imageArr.count > 1 {
                for i in imageArr{
                    guard let data: Data = i.jpegData(compressionQuality: 0.5)else{return}//UIImageJPEGRepresentation(image, 0.5)!
                    let name = Int(Date().timeIntervalSince1970)
                    let uploadData = MultipartObject(data: data, name: imageName, fileName: "\(name).jpg", mimeType: "image/jpg")
                    imageData.append(uploadData)
                }
            }
            JSONManager.shared.upLoadApi(apiUrl,uploadObjects: imageData, type: type, parameter: parameters) { (result, error) in
//                 Constant.Loader.Hide()
                 Indicator.shared().stop()

                guard error == nil else {
                    Vc.showAlert(message: (error?.localizedDescription)!, strtitle: "Error")
                    return }
                guard let data = result else {return}
                do {
                    let obj = try JSONDecoder().decode(T.self, from: data as! Data)
                    completion(obj)
                }
                catch let jsonError {
                    print("failed to dowload data", jsonError)
                    Vc.showAlert(message: "Error", strtitle: jsonError.localizedDescription)
                }
            }
        }
        else {
            Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
        }
    }
    
    
    
    class func postApiWithVideo<T : Decodable > (_ apiUrl : String, uploadKeyName: String,documentData: Data?,imageData : UIImage? ,videoData : Data?, Vc : UIViewController, parameters : [String : Any], completion : @escaping (T) -> Void) {
        //let data: Data = UIImageJPEGRepresentation(image, 0.5)!
        if JSONManager.isConnectedToInternet {
           // Constant.Loader.Show(view: Vc.view)
          //  Indicator.shared().start()
            let name = Int(Date().timeIntervalSince1970)
            var uploadData:[MultipartObject] = []
            if let imageData = imageData!.jpegData(compressionQuality: 0.8)
            {
                let imgData = MultipartObject(data: imageData, name: uploadKeyName, fileName: "thumbnail_image_\(name).png", mimeType: "image/png")
                uploadData.append(imgData)
            }
            if let data1 = videoData{
                let videoData = MultipartObject(data: data1, name: uploadKeyName, fileName: "video_\(name).mov", mimeType: "mov")
                uploadData.append(videoData)
            }
            if let data2 = documentData{
                let videoData = MultipartObject(data: data2, name: uploadKeyName, fileName: "\(name).pdf", mimeType: "mov")
                uploadData.append(videoData)
            }
            JSONManager.shared.upLoadApi(apiUrl, uploadObjects: uploadData, parameter: parameters) { (result, error) in
              //  Constant.Loader.Hide()
              //  Indicator.shared().stop()
                guard error == nil else {
                    Vc.showAlert(message: (error?.localizedDescription)!, strtitle: "Error")
                    return }
                guard let data = result else {return}
                do {
                    let obj = try JSONDecoder().decode(T.self, from: data as! Data)
                    completion(obj)
                }
                catch let jsonError {
                    print("failed to dowload data", jsonError)
              //      Vc.showAlert(message: "Error", strtitle: jsonError as! String)
                }
            }
        }
        else {
            Vc.showAlert(message: "No Internet Connected", strtitle: "Error")
        }
    }
}

// MARK:-   MultiPartObject Class
class MultipartObject : NSObject {
    var dataObj : Data! = nil
    var strName : String = ""
    var strFileName : String = ""
    var strMimeType : String = ""
    
    init(data: Data!, name: String!, fileName: String!, mimeType: String! ) {
        super.init()
        dataObj = data
        strName = name
        strFileName = fileName
        strMimeType = mimeType
    }
}

