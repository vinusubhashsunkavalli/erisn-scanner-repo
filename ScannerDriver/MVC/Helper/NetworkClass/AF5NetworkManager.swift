//
//  AF5NetworkManager.swift
//  ProjectTemplate
//
//  Created by vamsi on 29/03/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import Foundation
import Alamofire
import NVActivityIndicatorView

enum DataNotFound:Error {
    case dataNotFound
}

typealias successs = (Data)-> Void
typealias failure = (String)-> Void
typealias Response = [String:Any]

//var accessToken:String{
//    return "Bearer " + Defaults.user.accessToken
//}

//"authorization":"bearer"+ProjectHelpers.getToken()
func header()->HTTPHeaders{
//    let contentType = ["Content-Type":"application/json"]
//    if "Defaults.user.accessToken".isEmpty{
//        return contentType
//    }else{
        return [
           // "Accept": "application/json",
            "Content-Type":"application/json",
//            "authorization":"bearer "+ProjectHelpers.getToken()
            "Authorization":"Bearer "+ProjectHelpers.getToken()
            
//
//            "Authorization":"Bearer "+"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJpc3MiOiJodHRwczpcL1wvZmlzaGh1LmNvbVwvaGFybWFpa2FcLyIsInVzZXJuYW1lIjoiYWRpaWJveXMiLCJlbWFpbCI6ImFuYW5kaEBlcmlzbi5jb20iLCJwcm9maWxlX2ltYWdlIjoiIiwibG9jYXRpb24iOiJDdWRkYWxvcmUiLCJ0YWdfbmFtZSI6Im5vcm1hbCIsImlhdCI6MTYxMTIxMjYxMywiZXhwIjoxNjExODE3NDEzfQ.H87F9dxV8Yid3ILQ8D8A2dfUc2UU_DWxKN6nLGqi0Ao"
//                + "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJzdHVkZW50X25hbWUiOiJKb2huIERvZSIsImlhdCI6MTYxMTExNzU4OCwiZXhwIjoxNjExMTM1NTg4fQ.VMICRjInixAOhVcr88cYXnaBBWsRkYvWPPDhtFPWRpE"
          //  "authorization":"bearer " + Defaults.user.accessToken
            //eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJzdHVkZW50X25hbWUiOiJKb2huIERvZSIsImlhdCI6MTYxMTExNzU4OCwiZXhwIjoxNjExMTM1NTg4fQ.VMICRjInixAOhVcr88cYXnaBBWsRkYvWPPDhtFPWRpE
            
        ]
//    }
}

//Using Codable
//This will decode array and dictionay
class NetworkManager{

    private init(){}

    static func hitApi<T:Codable>(
        url:String,
        vc : UIViewController,
        paramaters:Response? = nil,
        header:HTTPHeaders = header(),
        httpMethodType:HTTPMethod = .get,
        success:@escaping(T)-> () ,
        failure: @escaping failure)
    {

         ProjectHelpers.forLoading(vc: vc)
        if Connectivity.isNotConnectedToInternet{
            failure(ATErrorMessage.Server.checkConnection)
        }
        AF.request(url, method: httpMethodType, parameters: paramaters, headers: header).responseJSON { (response) in
            switch response.result{
            case .success(let value):
                guard let responseData = response.data else{
                    failure(ATErrorMessage.Server.DataNotFound)
                    return
                }
                guard let statusCode = response.response?.statusCode,statusCode == 200 else{
                    //checking if response is in dictionay or json else Array
                    if let responseDict = value as? Response {
                        print(responseDict)
                        if let errorMessage = responseDict["message"] as? String{
                            ProjectHelpers.stopLoading(vc: vc)
                            vc.showAlert(for: errorMessage)
                            
                            failure(errorMessage)
                        }
                    }
                    failure(ATErrorMessage.Server.DataNotFound)
                    return
                }
                do{
                    let result = try JSONDecoder().decode(T.self, from: responseData)
                    success(result)
                }catch let err{
                    print(err)
                    failure(err.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
                failure(error.localizedDescription)
            }
              ProjectHelpers.stopLoading(vc: vc)
        }
      
    }
}

extension NetworkManager{
    static func multipart(parameters: Parameters?, image: UIImage? = nil, fileData : Data? = nil, fileExt : String = "", videoData : Data? = nil, gifData : Data? = nil, audioData : Data? = nil, key: String = "", fileName: String = "", url: String, success: @escaping (_ responseData: Response) -> (), failure: @escaping failure, progressHandler:((_ progress: Progress) -> Void)? = nil) {
        var data : Data? = nil
        var extenionName = ""

        if image != nil{
            data = image!.jpegData(compressionQuality: 1.0)
            extenionName = ".jpg"
        }
        if videoData != nil{
            data = videoData!
            extenionName = ".mp4"
        }
        if gifData != nil{
            data = gifData!
            extenionName = "gif"
        }
        if audioData != nil{
            data = audioData!
            extenionName = "m4a"
        }

        if fileData != nil {
            data = fileData!
            extenionName = fileExt
        }

        let imageName = "UserObject.userObj.username" + extenionName
        AF.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(data!, withName: "image", fileName: imageName, mimeType: (image != nil) ? "image/jpg/jpeg" : ((videoData != nil) ? "video/mp4/MP4" : (gifData != nil) ? "image/gif" : (audioData != nil) ? "audio/m4a" : (fileData != nil) ? "application/pdf/mspowerpoint/msword/.doc/.ppt/.pdf" : "") )}, to: url,
                                                                                                                                                                                                                                                                                                                                 method: .put,
                                                                                                                                                                                                                                                                                                                                 headers: header())
            .uploadProgress(queue: .main, closure: { progress in
                //Current upload progress of file
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .response { resp in
                switch resp.result{
                case .failure(let error):
                    print(error)
                case.success( _):
                    print("🥶🥶Response after upload Img: (resp.result)")
                }
        }








        //        AF.upload(multipartFormData:{ multipartFormData in
        //            multipartFormData.append(data!, withName: "image", fileName: imageName, mimeType: (image != nil) ? "image/jpg/jpeg" : ((videoData != nil) ? "video/mp4/MP4" : (gifData != nil) ? "image/gif" : (audioData != nil) ? "audio/m4a" : (fileData != nil) ? "application/pdf/mspowerpoint/msword/.doc/.ppt/.pdf" : "") )},
        //                         usingThreshold:UInt64.init(),
        //                         to:url,
        //                         method:.put,
        //                         headers:header(),
        //                         encodingCompletion: { encodingResult in
        //                            switch encodingResult {
        //                            case .success(let upload, _, _):
        //                                upload.responseJSON { response in
        //                                    guard let dataResp = response.data else{
        //                                        assertionFailure("Failed to bind data")
        //                                        return
        //                                    }
        //                                    do{
        //                                        if let json = try JSONSerialization.jsonObject(with: dataResp, options: .mutableContainers) as? Response{
        //                                            if let statusCode = json["statusCode"] as? Int,
        //                                                statusCode == 200{
        //                                                success(json)
        //                                            }else{
        //                                                if let message = json["message"] as? String{
        //                                                    print(message)
        //                                                    failure("Video's size must be less than 25mb")
        //                                                }
        //                                            }
        //                                        }
        //                                    }catch{
        //                                        //                                        assertionFailure("Failed to bind data")
        //                                    }
        //                                }
        //                                upload.uploadProgress { (progress) in
        //                                    if let handler = progressHandler {
        //                                        handler(progress)
        //                                    }
        //                                }
        //                            case .failure(let error):
        //                                failure(error.localizedDescription)
        //                            }
        //        })
        //    }


    }
}
