//
//  Indicator.swift
//  Catalog
//
//  Created by Partap Rana on 15/04/20.
//  Copyright © 2020 Partap Rana. All rights reserved.
//



import Foundation
import UIKit
import NVActivityIndicatorView
  
class Indicator: UIViewController  {
    let size = CGSize(width:50, height: 50)
     
    class func shared() -> Indicator {
        struct Static {
            static let manager = Indicator()
        }
        return Static.manager
    }
    
   func start(){
      // showActivityIndicator(type: NVActivityIndicatorType.ballBeat, size: size)
       // startAnimating(size,message:"",type:NVActivityIndicatorType.ballBeat)
    }
    
    func start(msg:String){
        //startAnimating(size,message:msg,type:NVActivityIndicatorType.ballBeat)
    }
    
    func stop(){
        //stopAnimating()
    }
}

public protocol ActivityIndicatorPresenter {
    
    /// The activity indicator
    var activityIndicator: UIActivityIndicatorView { get }
    
    /// Show the activity indicator in the view
    func showActivityIndicator(type : NVActivityIndicatorType,
                               size : CGSize)
    
    /// Hide the activity indicator in the view
    func hideActivityIndicator()
}


public extension ActivityIndicatorPresenter where Self: UIViewController {
    
    func showActivityIndicator(type : NVActivityIndicatorType,
                               size : CGSize){
        DispatchQueue.main.async {
            
            self.activityIndicator.style = .whiteLarge
            self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 80, height: 80) //or whatever size you would like
            self.activityIndicator.center = CGPoint(x: self.view.bounds.size.width / 2, y: self.view.bounds.height / 2)
            self.view.addSubview(self.activityIndicator)
            self.activityIndicator.startAnimating()
        }
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.removeFromSuperview()
        }
    }
}
