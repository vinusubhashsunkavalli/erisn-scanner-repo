
//AF5MultipartUpload.swift
//ProjectTemplate
//
//Created by vamsi on 22/07/20.
//Copyright © 2020 vamsi. All rights reserved.


import Foundation
import Alamofire

extension NetworkManager{
    static func multipartImageUpload(httpMethodType:HTTPMethod = .post,parameters: Parameters?, image: [UIImage]? = nil,key: String = "img[]", fileName: String = "user_uploaded_images", url: String, success: @escaping (_ responseData: Response) -> (), failure: @escaping failure, progressHandler:((_ progress: Progress) -> Void)? = nil) {
         if Connectivity.isNotConnectedToInternet{
            failure(ATErrorMessage.Server.checkConnection)
            return
        }
        var data = [Data]()
        if image != nil{
            if let imagesArray = image{
                imagesArray.forEach({ (img) in
                    if let compressedImage = img.jpegData(compressionQuality: 0.5){
                        data.append(compressedImage)
                    }
                })
            }
        }
        AF.upload(multipartFormData: { multipartFormData in
            if let params = parameters{
                for (key, value) in params {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }

            for (index,_) in image!.enumerated(){
                multipartFormData.append(data[index], withName: key, fileName: fileName, mimeType: "image/jpg/jpeg"  )
            }

        }, to: url,
           method: httpMethodType,
           headers: header())
            .uploadProgress(queue: .main, closure: { progress in
                //Current upload progress of file
                print("Upload Progress: \(progress.fractionCompleted)")
               // progressHandler(progress)
            })
            .response { resp in
                switch resp.result{
                case .failure(let error):
                    failure(error.localizedDescription)
                case.success(let dataResponse):
                    guard let responseData = dataResponse else{
                        failure(ATErrorMessage.Server.DataNotFound)
                        return
                    }
                    guard let statusCode = resp.response?.statusCode,statusCode == 200 else{
                        failure(ATErrorMessage.Server.DataNotFound)
                        return
                    }
                    do{
                        if let resp = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? Response{
                            success(resp)
                        }
                    }catch{
                        failure(error.localizedDescription)
                    }
                }
        }
    }
}
