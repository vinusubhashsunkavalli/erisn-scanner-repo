//
//  Heeeyyy.swift
//  LMSProduct
//
//  Created by VinuSubhash Sunkavalli on 31/12/21.
//

//  ExtensionHelper.swift
//  Hanger
//
//  Created by Shivani on 03/01/19.
//  Copyright © 2019 Shivani. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import SDWebImage

extension NSLayoutDimension {
    
    @discardableResult
    func set(
        to constant: CGFloat,
        priority: UILayoutPriority = .required
    ) -> NSLayoutConstraint {
        
        let cons = constraint(equalToConstant: constant)
        cons.priority = priority
        cons.isActive = true
        return cons
    }
}

extension UIView {
    
  
    
    @IBInspectable
    var roundCorners: Bool {
        get {
            return false
        }
        set {
            layer.cornerRadius = newValue == true ? self.frame.height / 2 : 0.0
            layer.masksToBounds = newValue
        }
    }
    
    @IBInspectable
    var boarderWidth : CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
        
    func addShadow() {
        self.layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 3.0)
        layer.shadowRadius = 5
        layer.shadowOpacity = 0.5
    }
  
    
    @IBInspectable var underlineColour :UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            else {
                return nil
            }
        }
    }
    
    
    @IBInspectable var underLine : CGFloat {
        get {
            return 0.0
        }
        set {
            let border = CALayer()
            let width = CGFloat(newValue)
            border.borderColor = underlineColour?.cgColor
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width +
                self.frame.size.height, height: self.frame.size.height)
            border.borderWidth = width
            self.layer.addSublayer(border)
            self.layer.masksToBounds = true
        }
    }
    
    @IBInspectable var topCorners : CGFloat {
        get {
            return 0.0
        }
        set {
            self.layer.masksToBounds = false
            self.layer.cornerRadius = newValue
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
    
    @IBInspectable var bottomCorners : CGFloat {
        get {
            return 0.0
        }
        set {
            self.layer.masksToBounds = false
            self.layer.cornerRadius = newValue
            self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        }
    }
    
    @IBInspectable var rightCorners : CGFloat {
        get {
            return 0.0
        }
        set {
            self.layer.masksToBounds = false
            self.layer.cornerRadius = newValue
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        }
    }
    
    @IBInspectable var leftCorners : CGFloat {
        get {
            return 0.0
        }
        set {
            self.layer.masksToBounds = false
            self.layer.cornerRadius = newValue
            self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        }
    }
    
    func startRotating(duration: Double = 1) {
        let kAnimationKey = "rotation"
        if self.layer.animation(forKey: kAnimationKey) == nil {
            let animate = CABasicAnimation(keyPath: "transform.rotation")
            animate.duration = duration
            animate.repeatCount = Float.infinity
            animate.fromValue = 0.0
            animate.toValue = Float(.pi * 2.0)
            self.layer.add(animate, forKey: kAnimationKey)
        }
    }
    
    func stopRotating() {
        let kAnimationKey = "rotation"
        if self.layer.animation(forKey: kAnimationKey) != nil {
            self.layer.removeAnimation(forKey: kAnimationKey)
        }
    }
}

extension UINavigationController {
    
    
    
    func setNavBarHide() {
        self.navigationBar.isHidden = true
    }
    
    func setNavBarShow() {
        self.navigationBar.isHidden = false
    }
    
    func hideTabBar() {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func showTabBar() {
        self.tabBarController?.tabBar.isHidden = false
    }
}

extension UIImageView {
    func setImage(_ str : String, placeholder : UIImage? = UIImage()) {
        let completeUrl = str//serverBaseURL + "/" + str
        let url = URL(string: completeUrl.replacingOccurrences(of: " ", with: "%20"))
        self.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.sd_setImage(with: url, placeholderImage: placeholder, options: .continueInBackground, context: nil)
    }
}

extension UIView {
    func roundCorners(view :UIView, corners: UIRectCorner, radius: CGFloat){
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        view.layer.mask = mask
    }
    func roundTopCorners(roundCorners : CGFloat) {
        //        self.clipsToBounds = true
        //        self.layer.cornerRadius = roundCorners
        //        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topLeft , .topRight],
                                     cornerRadii: CGSize(width: 8, height: 8))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
//    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
//        layer.masksToBounds = false
//        layer.shadowColor = color.cgColor
//        layer.shadowOpacity = opacity
//        layer.shadowOffset = offSet
//        layer.shadowRadius = radius
//        
//        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//        layer.shouldRasterize = true
//        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
//    }
//    func changeUnderLineColor(color : UIColor?) {
//        //self.layer.sublayers?.first?.removeFromSuperlayer()
//        let border = CALayer()
//        let width = CGFloat(1)
//        border.borderColor = color?.cgColor
//        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width +
//            self.frame.size.height, height: self.frame.size.height)
//        border.borderWidth = width
//        self.layer.addSublayer(border)
//        self.layer.masksToBounds = true
//    }
    func boarderColor(color : UIColor?) {
        //self.layer.sublayers?.first?.removeFromSuperlayer()
        self.layer.borderColor = color?.cgColor
    }
}

//MARK:- UILabel Extension for Adding text on trailing
extension UILabel {
    
    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
        let readMoreText: String = trailingText + moreText
        
        let lengthForVisibleString: Int = self.vissibleTextLength
        let mutableString: String = self.text!
        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        let readMoreLength: Int = (readMoreText.count)
        let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
        let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedString.Key.font: self.font])
        let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
        answerAttributed.append(readMoreAttributed)
        self.attributedText = answerAttributed
    }
    
    var vissibleTextLength: Int {
        let font: UIFont = self.font
        let mode: NSLineBreakMode = self.lineBreakMode
        let labelWidth: CGFloat = self.frame.size.width
        let labelHeight: CGFloat = self.frame.size.height
        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        
        let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: font]
        let attributedText = NSAttributedString(string: self.text!, attributes: attributes as? [NSAttributedString.Key : Any])
        let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
        
        if boundingRect.size.height > labelHeight {
            var index: Int = 0
            var prev: Int = 0
            let characterSet = CharacterSet.whitespacesAndNewlines
            repeat {
                prev = index
                if mode == NSLineBreakMode.byCharWrapping {
                    index += 1
                } else {
                    index = (self.text! as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: self.text!.count - index - 1)).location
                }
            } while index != NSNotFound && index < self.text!.count && (self.text! as NSString).substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes as? [NSAttributedString.Key : Any], context: nil).size.height <= labelHeight
            return prev
        }
        return self.text!.count
    }
}

//MARK:- TextField Extension
extension UITextField {
    
//    @IBInspectable var placeHolderColor: UIColor? {
//        get {
//            return self.placeHolderColor
//        }
//        set {
//            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
//        }
//    }
    
    
    @IBInspectable var rightTImage : UIImage? {
        get
        {
            return self.rightTImage
        }
        set
        {
            if let image = newValue{
                rightViewMode = .always
                var imageView = UIImageView()
                if #available(iOS 13, *) {
                    imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 15))
                    imageView.widthAnchor.set(to: 40)
                } else {
                    imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 15))
                }
                imageView.contentMode = .scaleAspectFit
                imageView.image = image
                rightView = imageView
            }else {
                rightViewMode = .never
            }
        }
    }
    
    @IBInspectable var leftImage : UIImage? {
        get
        {
            return self.rightTImage
        }
        set
        {
            if let image = newValue{
                leftViewMode = .always
                var imageView = UIImageView()
                if #available(iOS 13, *) {
                    imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 15))
                    imageView.widthAnchor.set(to: 30.0)
                } else {
                    imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 15))
                }
                imageView.contentMode = .scaleAspectFit
                imageView.image = image
                leftView = imageView
            }else {
                rightViewMode = .never
            }
        }
    }
    
    @IBInspectable var leftPadding : CGFloat {
        get {
            return 0.0
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: self.frame.size.height))
            self.leftView = paddingView
            self.leftViewMode = .always
        }
    }
    
    @IBInspectable var rightPadding : CGFloat {
        get {
            return 0.0
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: self.frame.size.height))
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
  
    
    func underlined(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width + self.frame.size.height, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    func setLeftImage(img: UIImage) {
        leftViewMode = .always
        var imageView = UIImageView()
        if #available(iOS 13, *) {
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 15))
            imageView.widthAnchor.set(to: 30.0)
        } else {
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 15))
        }
        imageView.contentMode = .scaleAspectFit
        imageView.image = img
        leftView = imageView
    }
}


extension UIViewController {
    func showActionSheet(message: String, strtittle: String, actionTittle: [String], actionStyle: [UIAlertAction.Style], withHandler handler: [((UIAlertAction) -> Void)]?)
    {
        var actionSheetController: UIAlertController = UIAlertController()
        
        if message != "" || strtittle != ""
        {
            actionSheetController = UIAlertController(title: strtittle,
                                                      message: message,
                                                      preferredStyle: .actionSheet)
        }
        for i in 0..<actionTittle.count
        {
            actionSheetController.addAction(UIAlertAction(title: actionTittle[i],
                                                          style: actionStyle[i],
                                                          handler: handler?[i]))
        }
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
//    func encodeData(data:UserData) {
//        let encoder = JSONEncoder()
//        do {
//            let obj = try encoder.encode(data)
//            let jsonString = String(data: obj, encoding: .utf8)
//            print("JSON String : " + jsonString!)
//            Constant.userDefault.set(obj, forKey: "userData")
//        }
//        catch let error {
//            print("Some Error is \(error)")
//        }
//    }
    
    func hideNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
    }

    func unHideNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
        navColor()
    }
    
    func unHideHomeNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
        navHomeColor()
    }

    func navColor(){
        self.navigationController?.navigationBar.setBackgroundImage(nil, for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    func navHomeColor(){
        self.navigationController?.navigationBar.setBackgroundImage(nil, for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        //self.navigationController?.navigationBar.barTintColor = .black
        self.navigationController?.navigationBar.backgroundColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black,NSAttributedString.Key.font: UIFont(name: "Nunito-Bold", size: 22)!]
    }
}

//MARK:- RoundUpTo to decimal.
extension Double {
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func currency() -> String  {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        let str = numberFormatter.string(from: NSNumber(value:self)) ?? "0"
        return str
    }
}

extension Int {
    func getTime() -> String {
        let date = Date(timeIntervalSince1970: (Double(self) / 1000.0))
        print("date - \(date)")
        let dF = DateFormatter()
        dF.timeZone = NSTimeZone.local
        dF.dateFormat = "h:mm a"
        return dF.string(from: date)
    }
    
    func getDateInMilli(format : String) -> String {
        let date = Date(timeIntervalSince1970: (Double(self) / 1000.0))
        print("date - \(date)")
        let dF = DateFormatter()
        dF.timeZone = NSTimeZone.local
        dF.dateFormat = format
        return dF.string(from: date)
    }
    
    func getDateWorkShop() -> String {
        let date = Date(timeIntervalSince1970: (Double(self) / 1000.0))
        print("date - \(date)")
        let dF = DateFormatter()
        dF.timeZone = NSTimeZone.local
        dF.dateFormat = "EEEE,d"
        return dF.string(from: date)
    }
    
    func getDateObject() -> Date {
        let date = Date(timeIntervalSince1970: (Double(self) / 1000.0))
        print("date - \(date)")
        return date
    }
    
    func getDateForHistory() -> String {
        let date = Date(timeIntervalSince1970: (Double(self) / 1000.0))
        print("date - \(date)")
        let dF = DateFormatter()
        dF.timeZone = NSTimeZone.local
        dF.dateFormat = "MM/dd/yyyy"
        return dF.string(from: date)
    }
    
    func getbookedDate() -> String {
        let date = Date(timeIntervalSince1970: (Double(self) / 1000.0))
        print("date - \(date)")
        let dF = DateFormatter()
        dF.timeZone = NSTimeZone.local
        dF.dateFormat = "yyyy-MM-dd"
        return dF.string(from: date)
    }
    
    func getOrderDate() -> String {
        let date = Date(timeIntervalSince1970: (Double(self) / 1000.0))
        print("date - \(date)")
        let dF = DateFormatter()
        dF.timeZone = NSTimeZone.local
        dF.dateFormat = "MMM d, yyyy"
        return dF.string(from: date)
    }
    
    func getOrderTime() -> String {
        let date = Date(timeIntervalSince1970: (Double(self) / 1000.0))
        print("date - \(date)")
        let dF = DateFormatter()
        dF.timeZone = NSTimeZone.local
        dF.dateFormat = "h:mm a"
        return dF.string(from: date)
    }
    var msToSeconds: Double {
        return Double(self) / 1000
    }
}

extension TimeInterval {
    var minuteSecondMS: String {
        return String(format:"%02d:%02d", minute, second)
    }
    var minute: Int {
        return Int((self/60).truncatingRemainder(dividingBy: 60))
    }
    var second: Int {
        return Int(truncatingRemainder(dividingBy: 60))
    }
    var millisecond: Int {
        return Int((self*1000).truncatingRemainder(dividingBy: 1000))
    }
}

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
    
    func getOrderTime() -> String {
        let dF = DateFormatter()
        dF.timeZone = NSTimeZone.local
        dF.dateFormat = "h:mm a"
        return dF.string(from: self)
    }
    
    func getDatefromMilli(format : String) -> String {
        let dF = DateFormatter()
        dF.timeZone = NSTimeZone.local
        dF.dateFormat = format
        return dF.string(from: self)
    }
    
    func timeAgoSinceNow() -> String {
        // From Time
        let fromDate = self
        // To Time
        let toDate = Date()
        // Estimation
        // Year
        if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "year ago" : "\(interval)" + " " + "years ago"
        }
        // Month
        if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "month ago" : "\(interval)" + " " + "months ago"
        }
        // Day
        if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "day ago" : "\(interval)" + " " + "days ago"
        }
        // Hours
        if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "hour ago" : "\(interval)" + " " + "hours ago"
        }
        // Minute
        if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "minute ago" : "\(interval)" + " " + "minutes ago"
        }
        return NSLocalizedString("a moment ago", comment: "")
    }
}

extension Dictionary where Key == String
{
    func stringWithBlank(key : String) -> String
    {
        if let obj = self[key]
        {
            if obj is NSNull
            {
                return ""
            }
            return "\(obj)"
        }
        return ""
    }
    
    func stringWithBlankForKey(key : String) -> String
    {
        if let obj = self[key]
        {
            if obj is NSNull
            {
                return ""
            }
            return "\(obj)"
        }
        return ""
    }
    
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
}

extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
//    func isValidPassword() -> Bool {
//
//        let patterns = ["^(?=.*[A-Z]).*$", "^(?=.*[0-9]).*$", "^(?=.*[!@#%&-_=:;\"'<>,`~\\*\\?\\+\\[\\]\\(\\)\\{\\}\\^\\$\\|\\\\\\.\\/]).*$"]
//
//        let pattPred = NSPredicate(format:"SELF MATCHES %@", patterns)
//        return pattPred.evaluate(with: self)
//    }
    
    
    func checkPattern(pattern:String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: [])
            if regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil {
                return false
            }
        }
        catch {
            print("ERROR")
        }
        return true
    }
    
    func checkLanguage() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    func strikeThrough()->NSAttributedString {
        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
    
    func getSubstring(needle: String, beforeNeedle: Bool = false) -> String? {
        guard let range = self.range(of: needle) else { return nil }
        if beforeNeedle {
            return self.substring(to: range.lowerBound)
        }
        return self.substring(from: range.upperBound)
    }
    
    var byWords: [SubSequence] {
        var byWords: [SubSequence] = []
        enumerateSubstrings(in: startIndex..., options: .byWords) { _, range, _, _ in
            byWords.append(self[range])
        }
        return byWords
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

//MARK:- To check value at index
extension Collection where Indices.Iterator.Element == Index {
    subscript (exist index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension Date {
    
    func dateStringOnly() -> String
    {
        let dF = DateFormatter()
        dF.timeZone = NSTimeZone.local
        dF.dateFormat = "yyyy-MM-dd"
        return dF.string(from: self)
    }
    
    func stringTodate () -> Date {
        let str = dateStringOnly()
        let dF = DateFormatter()
        dF.dateFormat = "yyyy-MM-dd"
        return dF.date(from: str)!
    }
}

extension String {
    func stringTodate () -> Date {
        //let str = dateStringOnly()
        let dF = DateFormatter()
        dF.dateFormat = "yyyy-MM-dd"
        return dF.date(from: self)!
    }
}

extension UIApplication {
    class var statusBarBackgroundColor: UIColor? {
        get {
            return (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor
        } set {
            (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor = newValue
        }
    }
}

extension UITableView {
    func loadCell<T : UITableViewCell>(with identifier : T) -> T {
        //print(String(describing: T.self))
        return self.dequeueReusableCell(withIdentifier: String(describing: T.self)) as! T
    }
}

extension UICollectionView {
    func loadCell<T : UICollectionViewCell>(with identifier : T, at index : IndexPath) -> T {
        //print(String(describing: T.self))
        return self.dequeueReusableCell(withReuseIdentifier: String(describing: T.self), for: index) as! T
    }
}

extension UIViewController {
//    func checkAndPopToController <T : UIViewController>(with : T) -> T? {
//        for vc in self.navigationController?.children ?? [] {
//            if vc is T {
//                return vc as? T
//            }
//        }
//        return nil
//    }
}


extension UIViewController {
    // MARK: - SVProgressHud Methods
    
    
    func showAlert(message: String ,strtitle: String, handler:((UIAlertAction) -> Void)! = nil)
    {
        let alert = UIAlertController(title: strtitle, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.view.tintColor = UIColor.red
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: handler))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithHandler(message: String ,strtitle: String, handler:((UIAlertAction) -> Void)! = nil)
    {
        let alert = UIAlertController(title: strtitle,
                                      message: message,
                                      preferredStyle: .alert)
        
        if handler == nil
        {
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .default,
                                          handler: nil))
        }
        else
        {
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .default,
                                          handler: handler))
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithOkAndCancel(message: String ,strtitle: String, okTitle : String, cancel : String, handler:((UIAlertAction) -> Void)! = nil, handlerCancel:((UIAlertAction) -> Void)! = nil)
    {
        let alert = UIAlertController(title: strtitle,
                                      message:message,
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: cancel,
                                      style: .cancel,
                                      handler: handlerCancel))
        
        alert.addAction(UIAlertAction(title: okTitle,
                                      style: .default,
                                      handler: handler))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithCancel_Red(message: String ,strtitle: String, okTitle : String, cancel : String, handler:((UIAlertAction) -> Void)! = nil,Cancelhandler:((UIAlertAction) -> Void)! = nil)
    {
        let alert = UIAlertController(title: strtitle,
                                      message:message,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: okTitle,
                                     style: .destructive,
                                     handler: handler)
        
        
        alert.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: cancel,
                                         style: .default,
                                         handler: Cancelhandler)
        
        //cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showErorAlert(error: NSError , handler:((UIAlertAction) -> Void)! = nil)
    {
        let alert = UIAlertController(title: "",
                                      message:error.localizedDescription,
                                      preferredStyle: .alert)
        
        if handler == nil
        {
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .default,
                                          handler: nil))
        }
        else
        {
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .default,
                                          handler: handler))
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showNetworkErrorAlert()
    {
        let alertCont = UIAlertController(title: "Error!", message: "Network Error Occured!", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok".checkLanguage(), style: .default, handler: nil)
        alertCont.addAction(alertAction)
        self.present(alertCont, animated: true, completion: nil)
    }
    
    func getCurrentTimestamp() -> String {
        let timestamp = DateFormatter.localizedString(from: Date(), dateStyle: .medium, timeStyle: .full)
        return timestamp
    }
    
    
    func barButtons(titles:String){
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named:"leftArrow3"), for: .normal)
        button.addTarget(self, action:#selector(backMothod), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItems = [barButton]
        self.navigationItem.title = titles
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]

    }

    @objc func backMothod() {
        navigationController?.popViewController(animated: true)
    }
    
    
    func barButtonsOrder(titles:String){
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named:"leftArrow3"), for: .normal)
        button.addTarget(self, action:#selector(backMothodOrder), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItems = [barButton]
        self.navigationItem.title = titles
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]

    }
    @objc func backMothodOrder() {
        //appDelegate.sideMenu()
    }

}

//MARK:- String Extension

extension String {
    
    func getOrderDate(format: String) -> String {
        let dF = DateFormatter()
        dF.timeZone = NSTimeZone.local
        dF.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date = dF.date(from: self)
        let df2 = DateFormatter()
        df2.timeZone = NSTimeZone.local
        df2.dateFormat = format
        return df2.string(from: date!)
    }
    
    func hexStringToUIColor () -> UIColor {
        var cString:String = self.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat
        
        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}

extension DispatchQueue {
    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
}

class TemporaryMediaFile {
    var url: URL?
    
    init(withData: Data) {
        let directory = FileManager.default.temporaryDirectory
        let fileName = "\(NSUUID().uuidString).mp3"
        let url = directory.appendingPathComponent(fileName)
        do {
            try withData.write(to: url)
            self.url = url
        } catch {
            print("Error creating temporary file: \(error)")
        }
    }
    
    public var avAsset: AVAsset? {
        if let url = self.url {
            return AVAsset(url: url)
        }
        
        return nil
    }
    
    public func deleteFile() {
        if let url = self.url {
            do {
                try FileManager.default.removeItem(at: url)
                self.url = nil
            } catch {
                print("Error deleting temporary file: \(error)")
            }
        }
    }
    
    deinit {
        self.deleteFile()
    }
}

public class DynamicSizeTableView: UITableView
{
    override public func layoutSubviews() {
        super.layoutSubviews()
        if bounds.size != intrinsicContentSize {
            invalidateIntrinsicContentSize()
        }
    }
    override public var intrinsicContentSize: CGSize {
        return contentSize
    }
}

public class DynamicSizeCollectionView: UICollectionView
{
    override public func layoutSubviews() {
        super.layoutSubviews()
        if bounds.size != intrinsicContentSize {
            invalidateIntrinsicContentSize()
        }
    }
    override public var intrinsicContentSize: CGSize {
        return contentSize
    }
}

extension Int {
    var roundedWithAbbreviations: String {
        let number = Double(self)
        let thousand = number / 1000
        let million = number / 1000000
        if million >= 1.0 {
            return "\(round(million*10)/10)M"
        }
        else if thousand >= 1.0 {
            return "\(round(thousand*10)/10)K"
        }
        else {
            return "\(self)"
        }
    }
}

extension UINavigationController {

    func previousViewController() -> UIViewController? {
        guard viewControllers.count > 1 else {
            return nil
        }
        return viewControllers[viewControllers.count - 2]
    }
}

class CustomDashedView: UIView {

    @IBInspectable var dashWidth: CGFloat = 0
    @IBInspectable var dashColor: UIColor = .clear
    @IBInspectable var dashLength: CGFloat = 0
    @IBInspectable var betweenDashesSpace: CGFloat = 0

    var dashBorder: CAShapeLayer?

    override func layoutSubviews() {
        super.layoutSubviews()
        dashBorder?.removeFromSuperlayer()
        let dashBorder = CAShapeLayer()
        dashBorder.lineWidth = dashWidth
        dashBorder.strokeColor = dashColor.cgColor
        dashBorder.lineDashPattern = [dashLength, betweenDashesSpace] as [NSNumber]
        dashBorder.frame = bounds
        dashBorder.fillColor = nil
        if cornerRadius > 0 {
            dashBorder.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        } else {
            dashBorder.path = UIBezierPath(rect: bounds).cgPath
        }
        layer.addSublayer(dashBorder)
        self.dashBorder = dashBorder
    }
}
//MARK:- Textfield
extension UITextField{
    var isEmpty: Bool {
        return self.text?.trimmingCharacters(in: .whitespaces).count == 0 ? true: false
    }
    
    var isValidInputName : Bool {
        let RegEx =  "[a-z A-Z ]+"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: self.text!.lowercased())
    }
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text!.lowercased())
    }
   
    var isValidPassword: Bool {
        return self.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 >= 8 ? true: false
    }
    var isLimit: Bool {
        return self.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 >= 100 ? true: false
    }
    var isValidName: Bool {
        return self.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 >= 2 ?  true: false
    }
    var isValidPhone: Bool {
        let phoneRegex = "^[0-9]{6,15}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: self.text?.trimmingCharacters(in: .whitespacesAndNewlines))
    }
 
}
extension UIView{
    func makeRounderCorners(_ radius : CGFloat){
        self.layer.cornerRadius = radius
    }
}
