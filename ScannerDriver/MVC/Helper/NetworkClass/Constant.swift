//
//  Constants.swift
//  FavBites
//
//  Created by jatin-pc on 8/30/17.
//  Copyright © 2017 Orem. All rights reserved.
//
import Foundation
import UIKit
//import GoogleMaps
//import GooglePlaces
var currencytype = String()
var MydeviceToken = "h"

struct Constant {
    // Common Class
   // static let dataModel = DataModel()
    static var KAlert = "Alert"
    static let userDefault = UserDefaults.standard
    //static var userData : UserData?
    static let placholderImage = #imageLiteral(resourceName: "userPlceholder")
    static let placholderImage1 = #imageLiteral(resourceName: "ProfilePlacehol")
    static let largePlacholderImage = UIImage(named: "largePlaceholder")
    
    static var token : String {
        get {
            if let token = userDefault.value(forKey: "token") as? String {
                return token
            }
            else {
                return ""
            }
        }
    }
    
    static var fcmToken : String {
        get {
            if let fcmToken = userDefault.value(forKey: "fcmToken") as? String {
                return fcmToken
            }
            else {
                return "adas"
            }
        }
    }
}
