//
//  Connectivity.swift
//  ProjectTemplate
//
//  Created by vamsi on 02/02/20.
//  Copyright © 2020 vamsi. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    static var isNotConnectedToInternet:Bool{
        return !NetworkReachabilityManager()!.isReachable
    }
}
