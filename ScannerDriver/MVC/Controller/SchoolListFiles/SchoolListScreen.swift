//
//  SchoolListScreen.swift
//  ScannerDriver
//
//  Created by VinuSubhash Sunkavalli on 01/06/22.
//

import UIKit
import NVActivityIndicatorView

class SchoolListScreen: UIViewController {
    
    @IBOutlet weak var activityView: NVActivityIndicatorView!
   // var activityView: NVActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var thisImage: UIImageView!
    @IBOutlet var thisGesturerecognizer: UISwipeGestureRecognizer!
    
    var urlString = "https://api.blackboard.com.np/getList"
    var searchDataArray =  [SchoolListModelInside]()
    var searchDataBackupArray =  [SchoolListModelInside]()
   // let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        getDataURLSession()
       // refreshing()
        //callAPI()
        //loadStaticData()
        
        
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tapGesture)
        
        tapGesture.cancelsTouchesInView = false
        
        thisGesturerecognizer.direction = .down
    }
    
    
    @IBAction func actionGesture(_ sender: UISwipeGestureRecognizer) {
       // refreshing()
        
      
//        activityView.frame = CGRect.init(x: 0, y: 0, width: 100, height: 200)
//        activityView.backgroundColor = UIColor.black     //give color to the view
//        activityView.center = self.view.center
        activityView.color = .gray
        activityView.startAnimating()
     //   self.thisImage.addSubview(activityView)
        
        
        tableView.reloadData()
        getDataURLSession()
        print("im swipped")
        
    }
    
//    func refreshing(){
//        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
//         refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
//         view.addSubview(refreshControl)
//        //view.addSubview()
//    }
//
//    @objc func refresh(_ sender: AnyObject) {
//       // Code to refresh table view
//        print("This is refreshed")
//        getDataURLSession()
//    }
    
    
//    func loadStaticData(){
//        let searchArray = Bundle.main.decode("HomeJson.json")
//        if let data = searchArray.data{
//            //            searchDataArray = data
//            searchDataBackupArray = data
//            //            DispatchQueue.main.async {
//            //                self.tableView.reloadData()
//            //            }
//        }
//    }
    
    
    
    func getDataURLSession()
    {
        
        
        
        print("this is called from Normal URL Session")
        
        //        let header: HTTPHeaders = [
        //            "Content-Type": "application/json",
        //            "Session": "fb4e7f9b-0f31-4709-"
        //        ]
        var urlRequest = URLRequest(url: URL(string: urlString)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Double.infinity)
        
        urlRequest.allHTTPHeaderFields = [
            "Content-Type": "application/json",
            // "Session": "fb4e7f9b-0f31-4709-",
            "Accept-Encoding" : "gzip, deflate, br",
            "Accept" : "*/*",
            "x-apikey" : "b08cff109464c8e033eacf5eee61250c",
            //  "x-api-key" : ""
        ]
        
        urlRequest.httpMethod = "POST"//.get//, .post, .put
        
        print("headers")
        print(urlRequest.allHTTPHeaderFields)
        
        URLSession.shared.dataTask(with: urlRequest) { [self] (data, response, error) in
            if let error = error {
                print(error)
            }
            else if let data = data ,let responseCode = response as? HTTPURLResponse {
                do {
                    
                    var thisOne = try JSONDecoder().decode(SchoolListModel.self, from: data)
                    print("*************************")
                    print(thisOne)
                    
                    if thisOne.status == true{
                        
                        let imgString = thisOne.data![0].logo
                        let xc = imgString!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                        let imgURL = URL(string: xc!)
                        
                        
                        
                        
                        DispatchQueue.main.async {
                            self.thisImage.sd_setImage(with: imgURL)
                            self.activityView.stopAnimating()
                        }
                        
                       
                        self.searchDataBackupArray = thisOne.data!
                    }else{
                        
                    }
                    
                    print("*************************")
                    // Parse your response here.
                }
                
                catch let parseJSONError {
                    print("error on parsing request to JSON : \(parseJSONError)")
                }
            }
        }.resume()
        
        
        
        
    }
    
    
    
    
    
    
//    func callAPI(){
//        NetworkManager.hitApi(url: "https://api.blackboard.com.np/getList", paramaters: nil, httpMethodType: .post) { (resp:SearchData) in
//            print("resp",resp)
//            print(resp.data?.count)
////            if let data = resp.data{
////                self.searchDataBackupArray = data
////            }
//        } failure: { err in
//            print("err",err)
//        }
//
//
//
//        var semaphore = DispatchSemaphore (value: 0)
//
//        var request = URLRequest(url: URL(string: "https://api.blackboard.com.np/getList")!,timeoutInterval: Double.infinity)
//        request.addValue("b08cff109464c8e033eacf5eee61250c", forHTTPHeaderField: "x-apikey")
//        request.addValue("ci_session=21ae1e1aa8e91dd14c96ddc48a8821bde7261775", forHTTPHeaderField: "Cookie")
//
//        request.httpMethod = "POST"
//
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//            guard let data = data else {
//                print(String(describing: error))
//                semaphore.signal()
//                return
//            }
//            print(String(data: data, encoding: .utf8)!)
//            semaphore.signal()
//        }
//
//        task.resume()
//        semaphore.wait()
//    }
    
    
}
extension SchoolListScreen:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchDataArray.count == 0{
            
            tableView.setEmptyMessage("Welcome to Blackboard Security Guard App",fontColor: .black)
        }else{
            tableView.restore()
            
            
        }
        
        return searchDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath) as? SearchTableViewCell else {return .init()}
        cell.containerView.layer.borderWidth = 0.5
        cell.containerView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        let data = searchDataArray[indexPath.row]
        cell.titleLbl.text = data.schoolName ?? ""
        if let urlStr = data.logo{
            let xc = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            urlStr.replacingOccurrences(of: " ", with: "%20")
            if let url = URL(string: xc!){
                cell.imgView.sd_setImage(with: url)
                cell.imgView.layer.borderWidth = 0.5
                cell.imgView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
                
            }
        }
        if indexPath.row == 0{
            print("search data from tableview cell")
            print(searchDataArray)
        }
    
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        UserDefaults.standard.set(true, forKey: UserdefaultsConstants.isSchoolSelected)
        
        UserDefaults.standard.set(searchDataArray[indexPath.row].schoolBaseURL+"api/", forKey: UserdefaultsConstants.baserURLConstant)
        
        //let url = UserdefaultsConstants.schoolLogoURL.replacingOccurrences(of: " ", with: "%20")
        let xc = searchDataArray[indexPath.row].logo!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        UserDefaults.standard.set(xc, forKey: UserdefaultsConstants.schoolLogoURL)
        UserDefaults.standard.set(searchDataArray[indexPath.row].schoolName, forKey: UserdefaultsConstants.schoolNameDefaults)
        
        
        
        let vc = loginStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        vc.modalPresentationStyle = .fullScreen
       // self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
        
        
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        let vc = loginStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//        self.window?.rootViewController = vc
//        self.window?.makeKeyAndVisible()

        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

//extension Bundle {
//    func decode(_ file: String) -> SearchData {
//        guard let url = self.url(forResource: file, withExtension: nil) else {
//            fatalError("Failed to locate \(file) in bundle.")
//        }
//        
//        guard let data = try? Data(contentsOf: url) else {
//            fatalError("Failed to load \(file) from bundle.")
//        }
//        
//        let decoder = JSONDecoder()
//        
//        guard let loaded = try? decoder.decode(SearchData.self, from: data) else {
//            fatalError("Failed to decode \(file) from bundle.")
//        }
//        
//        return loaded
//    }
//}

extension SchoolListScreen:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchDataArray = searchDataBackupArray
        self.searchDataArray = self.searchDataArray.filter { $0.schoolName.range(of: searchText, options: .caseInsensitive, range: nil) != nil }
        if searchText.isEmpty {
            self.searchDataArray.removeAll()
        }
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.searchDataArray = self.searchDataBackupArray
    }
}




struct SchoolListModel: Codable {
    let status: Bool
    let data: [SchoolListModelInside]?
    let message: String
}

// MARK: - Datum
struct SchoolListModelInside: Codable {
    let id, schoolName: String
    let schoolBaseURL: String
    let authKey: String
    let logo: String?
    let isActive, created: String
  //  let modified: JSONNull?

    enum CodingKeys: String, CodingKey {
        case id
        case schoolName = "school_name"
        case schoolBaseURL = "school_base_url"
        case authKey = "auth_key"
        case logo
        case isActive = "is_active"
        case created
        //, modified
    }
}
