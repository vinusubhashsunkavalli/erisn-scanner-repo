//
//  CustomTabbarVC.swift
//  ScannerDriver
//
//  Created by vinnu subhash on 25/08/21.
//

import UIKit


protocol CustomTabBarControllerDelegate {
    func onTabSelected(isTheSame: Bool)
}


class CustomTabbarVC: UITabBarController, UITabBarControllerDelegate{ // View Did Load
override func viewDidLoad() {
    super.viewDidLoad()

    tabBar.backgroundColor = UIColor(rgb: 0x032DA1)
    tabBar.barTintColor = UIColor(rgb: 0x032DA1)
    tabBar.unselectedItemTintColor = .white
    tabBar.tintColor = .white
    self.selectedIndex = 1
    
   // self.tabBarController!.tabBar.backgroundColor = .systemPink
    //self.tabBarController.tabBar.barTintColor =  #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)
}

// Tab Bar Specific Code
override func viewDidAppear(_ animated: Bool) {
//    let controller1 = UIViewController(self.view.backgroundColor = UIColor.white)
//    controller1.tabBarItem = UITabBarItem(tabBarSystemItem: UITabBarSystemItem.contacts, tag: 1)
//    let nav1 = UINavigationController(rootViewController: controller1)
//
//    let controller2 = UIViewController()
//    controller2.tabBarItem = UITabBarItem(tabBarSystemItem: UITabBarItem.SystemItem.contacts, tag: 2)
//    let nav2 = UINavigationController(rootViewController: controller2)
//
//    let controller3 = UIViewController()
//    let nav3 = UINavigationController(rootViewController: controller3)
//    nav3.title = ""
//
//    let controller4 = UIViewController()
//    controller4.tabBarItem = UITabBarItem(tabBarSystemItem: UITabBarItem.SystemItem.contacts, tag: 4)
//    let nav4 = UINavigationController(rootViewController: controller4)
//
//    let controller5 = UIViewController()
//    controller5.tabBarItem = UITabBarItem(tabBarSystemItem: UITabBarItem.SystemItem.contacts, tag: 5)
//    let nav5 = UINavigationController(rootViewController: controller5)
//
//    self.viewControllers = [nav1, nav2, nav3, nav4, nav5]
    self.setupMiddleButton()
}

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
           (viewController as? CustomTabBarControllerDelegate)?.onTabSelected(isTheSame: selectedViewController == viewController)
           return true
       }
    
    
// TabBarButton – Setup Middle Button
func setupMiddleButton() {
    let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 75, height: 75))
    var menuButtonFrame = menuButton.frame
    menuButtonFrame.origin.y = self.view.bounds.height - menuButtonFrame.height - 50
    menuButtonFrame.origin.x = self.view.bounds.width / 2 - menuButtonFrame.size.width / 2
    menuButton.frame = menuButtonFrame

    //menuButton.backgroundColor = UIColor.red
    menuButton.layer.cornerRadius = menuButtonFrame.height/2
    self.view.addSubview(menuButton)
    self.delegate = self
    menuButton.setImage(UIImage(named: "QRTab"), for: UIControl.State.normal)
    menuButton.addTarget(self, action: #selector(CustomTabbarVC.menuButtonAction), for: UIControl.Event.touchUpInside)

    self.view.layoutIfNeeded()
}

// Menu Button Touch Action
    @objc func menuButtonAction(sender: UIButton) {
    self.selectedIndex = 1
    // console print to verify the button works
    print("Middle Button was just pressed!")
   }
    
    override func viewWillLayoutSubviews() {
          super.viewWillLayoutSubviews()

         let newTabBarHeight = tabBar.frame.height + 8.0

          var newFrame = tabBar.frame
          newFrame.size.height = newTabBarHeight
          newFrame.origin.y = view.frame.size.height - newTabBarHeight

          tabBar.frame = newFrame
      }
    
    
 }


class CustomTab: UITabBar {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       // layer.backgroundColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        
        
       // let newTabBarHeight = view + 16.0
        
        
        layer.masksToBounds = true
        layer.cornerRadius = 20
        layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
    }
    
 
    
    
}
