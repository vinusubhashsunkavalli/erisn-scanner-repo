//
//  ScannerVC.swift
//  ScannerDriver
//
//  Created by vinnu subhash on 26/08/21.
//

import UIKit
import AVFoundation
import NepaliDatePicker

class ScannerVC: UIViewController,AVCaptureMetadataOutputObjectsDelegate {
    
    
    var captureSession: AVCaptureSession!
       var previewLayer: AVCaptureVideoPreviewLayer!
    
    
    @IBOutlet weak var scanView: UIView!
    @IBOutlet weak var scannedLabel: UILabel!
    @IBOutlet weak var scannedImage: UIImageView!
    @IBOutlet weak var qrcodeScanner: UILabel!
    @IBOutlet weak var scanNextbtn: UIButton!
    
    
    var thisData : ScannerModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        scanNextbtn.cornerRadiusMethod(25)
        scannedImage.isHidden = true
        scanNextbtn.isHidden = true
        setupQRCode()
    }
    
    
    func setupQRCode(){
        
        captureSession = AVCaptureSession()
        
        
        scannedLabel.isHidden = true
        scanView.cornerRadiusMethod(15)
        scanView.backgroundColor = .black
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        scanView.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
        
        
        
        
        
        
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)

        self.scannedLabel.isHidden = !false
        self.scannedImage.isHidden = !false
        
          if (captureSession?.isRunning == false) {
              captureSession.startRunning()
          }
      }

      override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)

          if (captureSession?.isRunning == true) {
              captureSession.stopRunning()
          }
      }
    
    @IBAction func scanBtnAction(_ sender: UIButton) {
        
        
        scannedLabel.isHidden = true
        scannedImage.isHidden = true
        
        setupQRCode()
        
    }
    
    
    func failed() {
          let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
          ac.addAction(UIAlertAction(title: "OK", style: .default))
          present(ac, animated: true)
          captureSession = nil
      }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
         captureSession.stopRunning()

         if let metadataObject = metadataObjects.first {
             guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
             guard let stringValue = readableObject.stringValue else { return }
             AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
             found(code: stringValue)
            
            
          //  scannedLabel.text = stringValue
             
             
             print("this is the scanned value")
             print(stringValue)
             print("this is the scanned value")
             
             gettingData(studentid: stringValue)
            // postData(studentid: stringValue)
           // postDataURLSession(studentid: stringValue)
             
            
         }

        // dismiss(animated: true)
     }
    
    
    func found(code: String) {
          print(code)
          self.navigationController?.popViewController(animated: true)
      }

      override var prefersStatusBarHidden: Bool {
          return true
      }

      override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
          return .portrait
      }
    
    
    


}


extension ScannerVC{
    
    
    
    
    
        func gettingData(studentid:String){

            
            let formatter = DateFormatter()
            //formatter.locale = Locale(identifier: "en_US_POSIX")
           // formatter.locale = Locale(identifier: "ne")
            formatter.dateFormat = "hh:mm a"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"

            let dateString = formatter.string(from: Date())
            print(dateString)
            
            let formattery = DateFormatter()
            formattery.dateFormat = "yyyy-MM-dd"
            
            let date = Date()
            let calendar = Calendar.current
            let currentEnglishYear = calendar.component(.year, from: date)
            let currentEnglishMonth = calendar.component(.month, from: date)
            let currentEnglishDay = calendar.component(.day, from: date)
            
            let currentNepaliDate = DateConverter().getNepaliDate(englishDate: DateModel(year: currentEnglishYear, month: currentEnglishMonth, day: currentEnglishDay))
            
            guard let year = currentNepaliDate?.year, let month = currentNepaliDate?.month, let day = currentNepaliDate?.day else {return}
            
            let nepalDate = "\(year)-\(month)-\(day)"
            

            let params : [String:String] = [
                "student_id" : studentid,
               "date":nepalDate,
               "time":dateString,
            ]




      



            //APIs.uploadImagesAssignment

            JSONManager.postApiWithMultipleImage("addcheckinoutinfo",type: .post, imageArr: [], imageName: "images[]", Vc: self,parameters: params) { (model:ScannerModel ) in
                print("____________________________________")
                print(model)
                
                self.thisData = model
                DispatchQueue.main.async {
                    if self.thisData != nil{
                        
                        
                        
                       // self.scannedLabel.text = studentid
                        
                        
                        //self.scannedLabel.text = studentid
                        
                        
                        if self.thisData.status  == true{
                          
                            self.scannedImage.isHidden = false
                            self.scannedLabel.isHidden = false
                            self.scannedImage.image = UIImage(named: "tickGreen")
                            self.scannedLabel.text = "Scanned Successfully"
                            
                            
                            
                            self.scanNextbtn.setTitle("Scan Next", for: .normal)
                          //  self.qrcodeScanner.text = "Scanned Succesfully"
                        }else{
                            self.scannedLabel.text = self.thisData.message
                            self.scannedLabel.isHidden = false
                            self.scannedImage.isHidden = false
                          //  self.qrcodeScanner.text = "ID not recognized"
                            self.scannedImage.image = UIImage(named: "cancelRed")
                            self.scanNextbtn.setTitle("Retry", for: .normal)
                            //self.qrcodeScanner.text =
                        }
                        
                        
                        
                        
    //                    if self.thisData.status  == true{
    //                        self.scannedLabel.text = "Scanned Succesfully"
    //                       // self.qrcodeScanner.text = "Scanned Succesfully"
    //                    }else{
    //                        self.scannedLabel.text = self.thisData.message
    //                       // self.qrcodeScanner.text = self.thisData.message
    //                    }
                        
                        
                        self.scanNextbtn.isHidden = false
                        
                        
    //                    @IBOutlet weak var scannedLabel: UILabel!
    //                    @IBOutlet weak var scannedImage: UIImageView!
    //                    @IBOutlet weak var qrcodeScanner: UILabel!
    //                    @IBOutlet weak var scanNextbtn: UIButton!
    //
                        
                        
                        
                        
                    }else{
                        
                        
                       // self.scannedLabel.text = studentid
                        self.qrcodeScanner.text = "Scan Properly"
                        self.scanNextbtn.isHidden = true
                        
                    
                        
                        
                    }
                }
                
                
                
              //  self.thisTBV.reloadData()
                print("____________________________________")

              //  self.submitAssignmentJson()

            }

        }
    
    
    
    
    
    
    func postData(studentid:String){
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "hh:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"

        let dateString = formatter.string(from: Date())
        print(dateString)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let newDate = dateFormatter.string(from:  Date())
        print(newDate)
        
    
        
        let time = dateString
        let datey = newDate
        
        
        
        
        
        
        
        print("This is the time")
        print(ProjectHelpers.getToken())
        print(time)
        print("This is the time")
        print("This is the date")
        print(datey)
        print("This is the date")
        
        
        let formattery = DateFormatter()
        formattery.dateFormat = "yyyy-MM-dd"
        
        let date = Date()
        let calendar = Calendar.current
        let currentEnglishYear = calendar.component(.year, from: date)
        let currentEnglishMonth = calendar.component(.month, from: date)
        let currentEnglishDay = calendar.component(.day, from: date)
        
        let currentNepaliDate = DateConverter().getNepaliDate(englishDate: DateModel(year: currentEnglishYear, month: currentEnglishMonth, day: currentEnglishDay))
        
        guard let year = currentNepaliDate?.year, let month = currentNepaliDate?.month, let day = currentNepaliDate?.day else {return}
        
        let nepalDate = "\(year)-\(month)-\(day)"
        //nepaliBtnOut.setTitle(nepalDate, for: .normal)
        
        
        
        
        
        
        
        let parameters = [
          [
            "key": "student_id",
            "value": studentid,
            "type": "text"
          ],
          [
            "key": "date",
            "value": nepalDate,
            "type": "text"
          ],
          [
            "key": "time",
            "value": time,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try! NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        
        let urlllll = APIs.baseURL+"addcheckinoutinfo"
       // "https://dev.ezydemo.com/api/addcheckinoutinfo"
        var request = URLRequest(url: URL(string:urlllll )!,timeoutInterval: Double.infinity)
        request.addValue(ProjectHelpers.getToken(), forHTTPHeaderField: "x-api-key")
        request.addValue("ci_session=4163b008adcd07e579f1b2cdf67977edee07ca47", forHTTPHeaderField: "Cookie")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
           // semaphore.signal()
            return
          }
          print(String(data: data, encoding: .utf8)!)
            
            do{
                let myData: ScannerModel = try! JSONDecoder().decode(ScannerModel.self, from: data)
                self.thisData = myData
                print("im a success")
            }catch let err{
                print(err)
            }
            
            
            //MARK: Dispatch queue
            DispatchQueue.main.async {
                if self.thisData != nil{
                    
                    
                    
                   // self.scannedLabel.text = studentid
                    
                    
                    //self.scannedLabel.text = studentid
                    
                    
                    if self.thisData.status  == true{
                      
                        self.scannedImage.isHidden = false
                        self.scannedLabel.isHidden = false
                        self.scannedImage.image = UIImage(named: "tickGreen")
                        //self.scannedLabel.text = "Scanned Succesfully"
                        
                        
                        
                        self.scanNextbtn.setTitle("Scan Next", for: .normal)
                        //self.qrcodeScanner.text = "Scanned Succesfully"
                    }else{
                        self.scannedLabel.text = self.thisData.message
                        self.scannedLabel.isHidden = false
                        self.scannedImage.isHidden = false
                        self.scannedImage.image = UIImage(named: "cancelRed")
                        self.scanNextbtn.setTitle("Retry", for: .normal)
                        //self.qrcodeScanner.text =
                    }
                    
                    
                    
                    
//                    if self.thisData.status  == true{
//                        self.scannedLabel.text = "Scanned Succesfully"
//                       // self.qrcodeScanner.text = "Scanned Succesfully"
//                    }else{
//                        self.scannedLabel.text = self.thisData.message
//                       // self.qrcodeScanner.text = self.thisData.message
//                    }
                    
                    
                    self.scanNextbtn.isHidden = false
                    
                    
//                    @IBOutlet weak var scannedLabel: UILabel!
//                    @IBOutlet weak var scannedImage: UIImageView!
//                    @IBOutlet weak var qrcodeScanner: UILabel!
//                    @IBOutlet weak var scanNextbtn: UIButton!
//
                    
                    
                    
                    
                }else{
                    
                    
                   // self.scannedLabel.text = studentid
                    self.qrcodeScanner.text = "Scan Properly"
                    self.scanNextbtn.isHidden = true
                    
                
                    
                    
                }
            }
            
            
            
            
            
        //  semaphore.signal()
        }

        task.resume()
        
    }
    
    
    
    
    
    
}




struct ScannerModel: Codable {
    let status: Bool
   // let data: DataClass
    let message: String
}

