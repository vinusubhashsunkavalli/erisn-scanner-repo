//
//  FilterCell.swift
//  ScannerDriver
//
//  Created by vinnu subhash on 26/08/21.
//

import UIKit

class FilterCell: UITableViewCell {

    @IBOutlet weak var tickSymbol: UIImageView!
    @IBOutlet weak var thisLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


