//
//  FilterScreenVC.swift
//  ScannerDriver
//
//  Created by vinnu subhash on 26/08/21.
//

import UIKit
import Alamofire



var selectedString = ""

class FilterScreenVC: UIViewController {

    @IBOutlet weak var backBar: BackActionNavBarView!
    @IBOutlet weak var thisTBV: UITableView!
    
    var x = 0
    var thisData : GetclasessModel!
    var thisArray = ["All","10th Class","9th Class","8th Class","7th Class","6th Class"]
    var selectedRow = 0
    var id = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backViewMethod()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getData()
        tableviewSetUp(tableView: thisTBV)
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    
    
    fileprivate func backViewMethod() {
        //questionApi()
        
        backBar.backClosure = {
            print("Hi im clicked")
            
            self.navigationController?.popViewController(animated: true)
            
        }
    
    
    

}
}

extension FilterScreenVC : UITableViewDelegate,UITableViewDataSource
{
    
    func tableviewSetUp(tableView : UITableView)  {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 280
        tableView.rowHeight = UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if x==0{
            return 0
        }else{
            return thisData.data.courseData.count+1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        thisTBV.register(UINib(nibName: "FilterCell", bundle: nil), forCellReuseIdentifier: "FilterCell")
        let cell = thisTBV.dequeueReusableCell(withIdentifier: "FilterCell") as! FilterCell
        
        
       
        
       
        
      
        
        if indexPath.row > 0{
            let da = thisData.data.courseData[indexPath.row-1]
            cell.thisLabel.text = da.courseName
        }else{
            cell.thisLabel.text = "ALL"
        }
        
        
        if selectedRow == 0{
            cell.tickSymbol.isHidden = false
        }else{
            
            if indexPath.row == selectedRow{
                if indexPath.row == 0{
                cell.tickSymbol.isHidden = false
            }else{
                cell.tickSymbol.isHidden = true
            }
            }else{
                
                cell.tickSymbol.isHidden = true
            }
  

    }
        cell.tickSymbol.isHidden = true
        return cell
    }
        
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        if indexPath.row == 0{
            
            selectedRow = 0
            
            id = ""
            classID = ""
           // self.dismiss(animated: true)
        }else{
            
            let da = thisData.data.courseData[indexPath.row-1]
            selectedRow = indexPath.row-1
            id = da.id
            classID = da.id
          
           // self.dismiss(animated: true)
            
        }
        
        self.navigationController?.popViewController(animated: true)
        tableView.reloadData()
        
        
       // selectedString = thisArray[indexPath.row]
        
        if indexPath.row == 2{
//            let vc = moreStoryboard.instantiateViewController(withIdentifier: "MyGradesVC") as! MyGradesVC
//            //self.present(vc, animated: true)
//            self.navigationController?.pushViewController(vc, animated: true)
        }
        
  
    }
}



extension FilterScreenVC{
    
    func getData()
    {
        
        let apiKey = ProjectHelpers.getToken()
        
        let headers: HTTPHeaders = [
            "x-api-key": apiKey
        ]
        
    //    let params : [:] = [
    //    ]
        
        
        print("URL is")
        print(APIs.baseURL+APIs.profileAPI)
        print("URL is")
        print("This is the api key")
        print(apiKey)
        print("This is the api key")
        
        
        NetworkManager.hitApi(url: APIs.baseURL+APIs.classFilterAPI, vc: self, paramaters:[:],header: headers) { (model:GetclasessModel) in
            print(model)
          //  self.tableviewSetUp(tableView: self.thisTBV)
            
            self.thisData = model
            
            if model.status == true{
                self.x=1
            }else{
                self.x=0
                
            }
            
            
            DispatchQueue.main.async { [self] in
              //  self.tableviewSetUp(tableView: self.thisTBV)
                self.thisTBV.reloadData()
            }
        } failure: { e in
            print(e)
        }
    }
    
    
    
}


struct GetclasessModel: Codable {
    let status: Bool
    let data: GetclasessModelInside
    let message: String
}

// MARK: - DataClass
struct GetclasessModelInside: Codable {
    let courseData: [GetclasessModelInsideOne]
}

// MARK: - CourseDatum
struct GetclasessModelInsideOne: Codable {
    let id, courseName: String

    enum CodingKeys: String, CodingKey {
        case id
        case courseName = "course_name"
    }
}



