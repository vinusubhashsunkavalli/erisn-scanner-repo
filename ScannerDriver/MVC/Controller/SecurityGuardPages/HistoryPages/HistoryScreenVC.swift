//
//  HistoryScreenVC.swift
//  ScannerDriver
//
//  Created by vinnu subhash on 26/08/21.
//

import UIKit
import FSCalendar
import Alamofire
import NepaliDatePicker



var classID = ""

class HistoryScreenVC: UIViewController,FSCalendarDelegate ,FSCalendarDataSource ,FSCalendarDelegateAppearance,UITextFieldDelegate, CustomTabBarControllerDelegate,UITabBarDelegate,UITabBarControllerDelegate {

    
    
    
    @IBOutlet weak var dummyView: UIView!
    
    @IBOutlet weak var duplicateView: UIView!
    
    @IBOutlet weak var searchBtnOut: UIButton!
    
    @IBOutlet weak var calendarBtnOut: UIButton!
    
    @IBOutlet weak var filterOut: UIButton!
    
    @IBOutlet weak var thisTBV: UITableView!
    
    @IBOutlet weak var calenderview: FSCalendar!
    
    @IBOutlet weak var searchTF: UITextField!
    
    @IBOutlet weak var duplicateEntryTBV: UITableView!
    
    
    @IBOutlet weak var nepaliBtnOut: UIButton!
    @IBOutlet weak var nepaliHeight: NSLayoutConstraint!
    @IBOutlet weak var nepaliUIview: UIView!
    @IBOutlet weak var behindScreen: UIView!
    
    //var tabbarcontroller : UITabBarController!
    
    @IBOutlet weak var nepaliView: UIView!
    @IBOutlet weak var nepaliTBV: UITableView!
    
    
    var x = 0
    var y=0
    
    var tempArray = [CheckInOutDatum]()
    @IBOutlet weak var calendarTF: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    var thisData : OutInModel!
    var thisArray = [CheckInOutDatum]()
    var thisDuplicateData : DuplicateModel!
    var newDate = ""
    
    
    @IBOutlet weak var totalCounteeLabel: UILabel!
    @IBOutlet weak var thisLineView: UIView!
    // let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nepaliBtnOut.cornerRadiusMethod(8)
        
      // nepaliView.isHidden = true
       nepaliHeight.constant = 0
        setup()
        tabBarController?.delegate = self
        // Do any additional setup after loading the view.
        calenderview.isHidden = true
        thisTBV.isHidden = false
     
//        calendarTF.inputView = datePicker
//        calendarTF.delegate = self
        searchTF.delegate = self
        
        calenderview.placeholderType = .fillHeadTail
        BorderandShadow()
        
        
        if UserdefaultsConstants.isNepaliCalendar {
            nepaliBtnOut.isHidden = false
            nepaliBtnOut.isEnabled = true
        }else{
            nepaliBtnOut.isHidden = true
            nepaliBtnOut.isEnabled = false
            setyCalendarDefault()
        }
        
        behindScreen.backgroundColor = .clear
        behindScreen.isHidden = true
        dummyView.isHidden = true
        duplicateView.isHidden = true
        duplicateView.cornerRadiusMethod(20, opacity: 0.1, shwRadius: 1)
        thisTBV.isHidden = false
        
        let date = Date()
       

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        print(dateFormatter.string(from: date))
        
        
        
        if UserdefaultsConstants.isNepaliCalendar{
            
            
            nepalicalendarSetup()
            
        }else{
            
            hithit(date: dateFormatter.string(from: date))
            
        }
        
        
        
      //  datePicker.addTarget(self, action: #selector(dueDateChanged(sender:)), for: UIControl.Event.valueChanged)

        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //showDatePicker()
       // postData()
       // getData()
      
        
        if UserdefaultsConstants.isNepaliCalendar{
            
            
            
            
            if let text = nepaliBtnOut.titleLabel!.text{
                hithit(date:text)
            }
           
            //nepalicalendarSetup()
            
        }else{
        
            if let text = nepaliBtnOut.titleLabel!.text{
                hithit(date:text)
            }
            
        }
 
        
        
        
       // postData(date: dateFormatter.string(from: date))
        
        //MARK: Filter Technique
       //
        
    }
    
    
    func setNepaliDatePickerView() {
        let nepaliDatePicker = NepaliDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        nepaliDatePicker.delegate = self
        
     
        self.nepaliUIview.isHidden = false
        self.nepaliUIview.addSubview(nepaliDatePicker)
        self.addDoneToolbar(inView: self.nepaliUIview)
    }
    
    override func endEditing() {
        super.endEditing()
        print("Am i called")
        hithit(date: nepaliBtnOut.titleLabel!.text!)
        nepaliHeight.constant = 0
        view.reloadInputViews()
       // self.nepaliUIview.isHidden = true
    }
    
    
    
    @IBAction func nepaliBtnAction(_ sender: UIButton) {
        nepaliHeight.constant = 220
        self.setNepaliDatePickerView()
       //MARK: Uncomment the above code
     
        
    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
           print("tab bar is selected")
        setyCalendarDefault()
        let date = Date()
        searchTF.text = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        print(dateFormatter.string(from: date))
        classID = ""
      //  hithit(date: dateFormatter.string(from: date))
        
        if UserdefaultsConstants.isNepaliCalendar{
            
            
            nepalicalendarSetup()
            
        }else{
            
            hithit(date: dateFormatter.string(from: date))
            
        }
        
        
       }
    
  
    
    
    
    func nepalicalendarSetup(){
       // self.setNepaliDatePickerView()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let date = Date()
        let calendar = Calendar.current
        let currentEnglishYear = calendar.component(.year, from: date)
        let currentEnglishMonth = calendar.component(.month, from: date)
        let currentEnglishDay = calendar.component(.day, from: date)
        
        let currentNepaliDate = DateConverter().getNepaliDate(englishDate: DateModel(year: currentEnglishYear, month: currentEnglishMonth, day: currentEnglishDay))
        
        guard let year = currentNepaliDate?.year, let month = currentNepaliDate?.month, let day = currentNepaliDate?.day else {return}
        
        let nepalDate = "\(year)-\(month)-\(day)"
        nepaliBtnOut.setTitle(nepalDate, for: .normal)
        hithit(date:nepalDate)
       // let englishDate = DateUtils.getCurrentDateOnlyInString()
    }

    
    
    
    
    
    
    @IBAction func duplicateCancelAction(_ sender: UIButton) {
        
        behindScreen.isHidden = true
        duplicateView.isHidden = true
        dummyView.isHidden = true
        self.view.backgroundColor = UIColor.white
        
        
        
        thisTBV.isHidden = false
        y=0
        
        
        
    }
    func onTabSelected(isTheSame: Bool) {
           print("Tab1ViewController onTabSelected")
           //do something
       }
    
    
    
    func setyCalendarDefault(){
        
        
       // let picker : UIDatePicker = UIDatePicker()
        datePicker.datePickerMode = UIDatePicker.Mode.date
        // picker.addTarget(self, action: #selector(dueDateChanged(sender:)), for: UIControl.Event.valueChanged)
        //datePicker.
        datePicker.addTarget(self, action: #selector(dueDateChanged(sender:)), for: UIControl.Event.valueChanged)
        
        datePicker.date = Date()
//        let pickerSize : CGSize = datePicker.sizeThatFits(CGSize.zero)
//        datePicker.frame = CGRect(x:0.0, y:0.0, width:calendarBtnOut.frame.width, height:calendarBtnOut.frame.height)
//        // you probably don't want to set background color as black
//        // picker.backgroundColor = UIColor.blackColor()
//        self.calendarBtnOut.addSubview(datePicker)
        
        
        
        
    }
    
    @IBAction func searchyAction(_ sender: UIButton) {
        
        
        if !searchTF.text.isBlank{
           
            
            let filteredArray = self.tempArray.filter({($0.name.localizedCaseInsensitiveContains(searchTF.text!))})
            thisArray = filteredArray
            
            
            if thisArray.count == 0{
                self.showAlert(for: "No results found")
            }
            
            thisTBV.reloadData()
            
            
        }
        
        
        
    }
    
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date

       //ToolBar
       let toolbar = UIToolbar();
       toolbar.sizeToFit()
       let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
      let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

     toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

//        calendarTF.inputAccessoryView = toolbar
//        calendarTF.inputView = datePicker

     }
    
    @objc func donedatePicker(){

     let formatter = DateFormatter()
     formatter.dateFormat = "yyyy-MM-dd"
        calendarTF.text = formatter.string(from: datePicker.date)
        nepaliHeight.constant = 0
     self.view.endEditing(true)
   }

   @objc func cancelDatePicker(){
      self.view.endEditing(true)
    }
  
    
    
    
    
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        if textField == searchTF{
            
            if !textField.text.isBlank{
               
                
                let filteredArray = self.tempArray.filter({($0.name.localizedCaseInsensitiveContains(textField.text!))})
                thisArray = filteredArray
                
                if thisArray.count == 0{
                    self.showToast(message: "No Results found", font: .fontMediumFunc(size: 15))
                   // self.showAlert(for: "No results found")
                }
                
                thisTBV.reloadData()
                
                
            }
            
        }else{
            
            
            
        }
        
        
    
        
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == searchTF{
            
            if !textField.text.isBlank{
               
                
                let filteredArray = self.tempArray.filter({($0.name.localizedCaseInsensitiveContains(textField.text!))})
                thisArray = filteredArray
                if thisArray.count == 0{
                    
                    self.showToast(message: "No Results found", font: .fontMediumFunc(size: 15))
                    
                    //self.showAlert(for: "No results found")
                }
                
                thisTBV.reloadData()
                
                
            }
            
        }
        
        return true
    }
    
    
    func setup(){
        
       // tabbarcontroller.delegate = self
        
        
        calenderview.delegate = self
        calenderview.dataSource = self
        
        searchBtnOut.cornerRadiusMethod(26, opacity: 0.0, offx: 0, offy: 0, shwRadius: 0, sahdowColor: .gray)
        searchBtnOut.borderWidth = 2
        searchBtnOut.borderColor = .gray
        
        
        searchBtnOut.semanticContentAttribute = .forceRightToLeft
        searchBtnOut.setImage(UIImage(named: "searchIcon"), for: .normal)
        searchBtnOut.setTitle("Search", for: .normal)
        
        searchBtnOut.titleEdgeInsets = UIEdgeInsets(top: 0, left: -searchBtnOut.frame.width/2-20 , bottom: 0, right: 0 )
        searchBtnOut.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: -searchBtnOut.frame.width/2-30)
        
        
        
      //  lineView.lineFunction(percentage: 90, color: .appThemeColor, otherColor: .lightText)
        
        tableviewSetUp(tableView: thisTBV)
    
        
    }
    
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
         print(date)
        let ks = date.toLocalTime()
        let strArray = "\(ks)".components(separatedBy: " ")
        calendarBtnOut.setTitle("\(strArray[0])", for: .normal)
        
         thisTBV.isHidden = false
         calenderview.isHidden = true
    }

    
    @IBAction func filterAction(_ sender: UIButton) {
        
        let vc = securityGuardStory.instantiateViewController(withIdentifier: "FilterScreenVC") as! FilterScreenVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func calendarAction(_ sender: UIButton) {
        
        
        
    
        
       // action()
        
//        calenderview.isHidden = false
//        thisTBV.isHidden = true
//
//        let dateFormatter = DateFormatter()
//
//        dateFormatter.dateStyle = DateFormatter.Style.short
//        dateFormatter.timeStyle = DateFormatter.Style.short
//
//        let strDate = dateFormatter.string(from: datePicker.date)
//        print("This Date \(strDate)")
        
        
        
    }
    
    
    @IBAction func searchAction(_ sender: UIButton) {
        
        
    }
    
    
    
    //MARK:Calendar Setup
    
    func BorderandShadow()
    {
       // calenderview.layer.borderWidth = 1
       // calenderview.layer.borderColor = UIColor.lightGray.cgColor
        calenderview.layer.cornerRadius = 15
        addShadowforCalendarview(view: calenderview)
    }
    func addShadowforCalendarview(view:UIView){
        view.layer.shadowRadius = 3.0
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.withAlphaComponent(0.1).cgColor
        view.layer.shadowOpacity = 0.5
    }
    
    
    @IBAction func previousmonth(_ sender: UIButton) {
        
        calenderview.setCurrentPage(getPreviousMonth(date: calenderview.currentPage), animated: true)
    }
    @IBAction func nextmonth(_ sender: UIButton) {
        calenderview.setCurrentPage(getNextMonth(date: calenderview.currentPage), animated: true)
    }
    
    // Calener backward and forward function.
    func getNextMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: 1, to:date)!
    }
    
    func getPreviousMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: -1, to:date)!
    }
    
    //Starting date of calender
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    
    
    
}

extension HistoryScreenVC:NepaliDatePickerDelegate{
    func pickerView(pickerView: UIView, selectedNepaliDate: String?) {
        nepaliBtnOut.setTitle(selectedNepaliDate, for: .normal)
       // self.nepaliDate.text =
    }
    
    func pickerView(pickerView: UIView, englishYear: Int, englishMonth: Int, englishDay: Int) {
      //  self.englishDate.text = "\(englishYear)-\(englishMonth)-\(englishDay)"
    }

    
    
    
}



extension HistoryScreenVC : UITableViewDelegate,UITableViewDataSource
{
    
    func tableviewSetUp(tableView : UITableView)  {
        duplicateEntryTBV.delegate = self
        duplicateEntryTBV.dataSource = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 280
        tableView.rowHeight = UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        //let size = view.frame.size
        
        if tableView == thisTBV{
            if indexPath.row == 0{
                return 70
            }else{
                return 50
            }
        }
        else{
            if indexPath.row == 0{
                return 50
            }else{
                return 30
            }
        }
        
       
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
        
        if tableView == thisTBV{
            
            
            if x == 0{
                thisTBV.setEmptyMessage("No Data Available")
                return 1
            }else{
                thisTBV.restore()
                return thisArray.count+1
              //  return thisData.data!.checkInOutData.count
            }
            
            
            
        }else{
            
            
            if y == 0{
                return 0
            }else{
                return thisDuplicateData.data.duplicateCheckInOutData.count+1
              //  return thisData.data!.checkInOutData.count
            }
            
            
        }
        
        
       
      
        
      //return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        
        if tableView == thisTBV{
            
//            if indexPath.row == 100{
//                thisTBV.register(UINib(nibName: "CalendarFilterTBVCell", bundle: nil), forCellReuseIdentifier: "CalendarFilterTBVCell")
//                let cell = thisTBV.dequeueReusableCell(withIdentifier: "CalendarFilterTBVCell") as! CalendarFilterTBVCell
//
//                let studentInfoScanned = thisData.data!.totalStudentScanned!
//                let studentInfoTotal = thisData.data!.totalStudent!
//
//                let percentage : Double = (Double(studentInfoScanned)/Double(studentInfoTotal))*100
//                cell.numberLabel.text = "\(studentInfoScanned)/\(studentInfoTotal)"
//                cell.percentage = percentage
//
//                print("This is the percentage :: \(percentage) ::")
//
//                cell.lineView.lineFunction(percentage: percentage, color: UIColor(rgb: 0x032DA1), otherColor: UIColor(rgb: 0xDFEBF1))
//                return cell
//            }
//
//            else
            if indexPath.row == 0{
               
                thisTBV.register(UINib(nibName: "InOutTBVCell", bundle: nil), forCellReuseIdentifier: "InOutTBVCell")
                let cell = thisTBV.dequeueReusableCell(withIdentifier: "InOutTBVCell") as! InOutTBVCell
                
                
                cell.name.text = "Name"
                cell.inlabel.text = "In "
                cell.outLabel.text = "Out"
                cell.name.textAlignment = .center
                cell.inlabel.textAlignment = .center
                cell.outLabel.textAlignment = .center
                cell.name.textColor = AppAssetConstant.KolorBlue
                cell.inlabel.textColor = AppAssetConstant.KolorBlue
                cell.outLabel.textColor = AppAssetConstant.KolorBlue
                cell.name.textColor = .black
                cell.inlabel.textColor = .black
                cell.outLabel.textColor = .black
                cell.thisImage.isHidden = true
                return cell
                
            }
            else{
                
                
                let da = thisArray[indexPath.row-1]
                
              //  let da = thisData.data.checkInOutData[indexPath.row-1]
                
                thisTBV.register(UINib(nibName: "InOutTBVCell", bundle: nil), forCellReuseIdentifier: "InOutTBVCell")
                let cell = thisTBV.dequeueReusableCell(withIdentifier: "InOutTBVCell") as! InOutTBVCell
                
                cell.name.textAlignment = .left
                cell.inlabel.textAlignment = .center
                cell.outLabel.textAlignment = .center
                cell.name.text = da.name
                cell.inlabel.text = da.checkInOutDatumIn
                cell.outLabel.text = da.out
                cell.name.textColor = .black
                cell.inlabel.textColor = .black
                cell.outLabel.textColor = .black
               
                
                if da.duplicateEntry == "No"{
                    cell.thisImage.isHidden = true
                }else{
                    cell.thisImage.isHidden = false
                }
            
                
                
                return cell
                
            }
            
            
            
        }else if tableView == duplicateEntryTBV{
            
            if indexPath.row == 0{
                
                duplicateEntryTBV.register(UINib(nibName: "InOutTBVCell", bundle: nil), forCellReuseIdentifier: "InOutTBVCell")
                let cell = thisTBV.dequeueReusableCell(withIdentifier: "InOutTBVCell") as! InOutTBVCell
                
                cell.name.text = "Name"
                cell.inlabel.text = "In"
                cell.outLabel.text = "Out"
                cell.name.textAlignment = .left
                cell.inlabel.textAlignment = .center
                cell.outLabel.textAlignment = .center
                
                
                cell.thisImage.isHidden = true
                return cell
                
            }
            else{
                
                
                let da = thisDuplicateData.data.duplicateCheckInOutData[indexPath.row - 1]
                
              //  let da = thisData.data.checkInOutData[indexPath.row-1]
                
                duplicateEntryTBV.register(UINib(nibName: "InOutTBVCell", bundle: nil), forCellReuseIdentifier: "InOutTBVCell")
                let cell = thisTBV.dequeueReusableCell(withIdentifier: "InOutTBVCell") as! InOutTBVCell
                
                cell.thisImage.isHidden = true
                cell.name.text = da.name
                cell.inlabel.text = da.duplicateCheckInOutDatumIn
                cell.outLabel.text = da.out
                cell.name.textAlignment = .left
                cell.inlabel.textAlignment = .center
                cell.outLabel.textAlignment = .center
                cell.name.font = .fontMediumFunc(size: 12)
                cell.outLabel.font = .fontMediumFunc(size: 12)
                cell.inlabel.font = .fontMediumFunc(size: 12)
                return cell
                
            }
            
            
            
            
        }
        else{
            
            
            let cell = thisTBV.dequeueReusableCell(withIdentifier: NepaliTBVC.reuseid, for: indexPath) as! NepaliTBVC
            
            
            cell.layoutIfNeeded()

            let storyboard = UIStoryboard(name: "NepaliStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CellViewController") as! CellViewController

            self.addChild(vc)
            cell.contentView.addSubview(vc.view)

            vc.view.translatesAutoresizingMaskIntoConstraints = false
            cell.contentView.addConstraint(NSLayoutConstraint(item: vc.view, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cell.contentView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1.0, constant: 0.0))
            cell.contentView.addConstraint(NSLayoutConstraint(item: vc.view, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cell.contentView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1.0, constant: 0.0))
            cell.contentView.addConstraint(NSLayoutConstraint(item: vc.view, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cell.contentView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0.0))
            cell.contentView.addConstraint(NSLayoutConstraint(item: vc.view, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cell.contentView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0.0))

            vc.didMove(toParent: self)
            vc.view.layoutIfNeeded()
            
            
            
            
            
            return cell
            
            
            
        }
        
        
        
        
     
    
      

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
//        selectedRow = indexPath.row
        tableView.reloadData()
        
        
        
        
        
        if indexPath.row > 0{
            let da = thisArray[indexPath.row-1]
            
            
            
            if da.duplicateEntry == "Yes"{
                
                if UserdefaultsConstants.isNepaliCalendar{
                  
                    let date = nepaliBtnOut.titleLabel!.text!
                    dupDataJSON(date: date, studentID: da.studentID)
                }else{
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                    let date = formatter.string(from: datePicker.date)
                    dupDataJSON(date: date, studentID: da.studentID)
                }
               
                
                
                //duplicateDataPost(studentID: da.studentID,datey: date)
                
                
            }else{
                
                
            }
            
          
            
            
            
            
//            let vc = moreStoryboard.instantiateViewController(withIdentifier: "MyGradesVC") as! MyGradesVC
//            //self.present(vc, animated: true)
//            self.navigationController?.pushViewController(vc, animated: true)
        }
        
  
    }
}




extension Date {
// Convert UTC (or GMT) to local time

func toLocalTime() -> Date {

let timezone = TimeZone.current

let seconds = TimeInterval(timezone.secondsFromGMT(for: self))

return Date(timeInterval: seconds, since: self)

}


    // Convert local time to UTC (or GMT)

    func toGlobalTime() -> Date {

    let timezone = TimeZone.current

    let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))

    return Date(timeInterval: seconds, since: self)

    }

    }



extension HistoryScreenVC {
    
    
    func getData(date : String = "")
    {
        
        let apiKey = ProjectHelpers.getToken()
        
        let headers: HTTPHeaders = [
            "x-api-key": apiKey
        ]
        
    //    let params : [:] = [
    //    ]
        let parameters = [
            "date": date,
            "class": classID
        ]
        
        print("This is the parameters")
        print(parameters)
        print("This is the parameters")
        print("URL is")
        print(APIs.baseURL+APIs.checkoutHistoryAPI)
        print("URL is")
        print("This is the api key")
        print(apiKey)
        print("This is the api key")
        
        
        NetworkManager.hitApi(url: APIs.baseURL+APIs.checkoutHistoryAPI, vc: self, paramaters:[:],header: headers) { (model:OutInModel) in
            print(model)
          //  self.tableviewSetUp(tableView: self.thisTBV)
            
            self.thisData = model
            
            if model.status == true{
                self.x=1
                
                //MARK: Change
                
                
                self.thisArray = model.data!.checkInOutData!
                
                self.tempArray = model.data!.checkInOutData!
                
                
                print("this is the array")
                print(self.thisArray)
                print("this is the array")
                
                
                DispatchQueue.main.async { [self] in
                  //  self.tableviewSetUp(tableView: self.thisTBV)
                    self.thisTBV.reloadData()
                }
                
                
            }else{
                self.x=0
                
            }
            
            
            DispatchQueue.main.async { [self] in
              //  self.tableviewSetUp(tableView: self.thisTBV)
                self.thisTBV.reloadData()
            }
        } failure: { e in
            print(e)
        }
    }
    
    
    
}

//MARK: Date picker
extension HistoryScreenVC{
    
    
    
    func action(btn : UIButton) {
  
   }
   @objc func dueDateChanged(sender:UIDatePicker){
       
       let dateFormatter = DateFormatter()
       dateFormatter.dateFormat = "yyyy-MM-dd"
       let newDate = dateFormatter.string(from:  datePicker.date)
       
       
       hithit(date: newDate)
       //postData(date: newDate)
       
      // getData(date: newDate)
      // getData(parameters: ["date":newDate])
       
       
       print(newDate)
       
       
       
//
//       let dateFormatter = DateFormatter()
//       dateFormatter.dateStyle = .short
//       dateFormatter.timeStyle = .none
      // calendarBtnOut.setTitle(dateFormatter.string(from: sender.date), for: .normal)
   }
    
    
    
    
    
}


extension HistoryScreenVC {
    
    
    
    
    func duplicateDataPost(studentID : String = "",datey : String = ""){
        
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "student_id",
            "value": studentID,
            "type": "text"
          ],
          [
            "key": "date",
            "value": datey,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try! NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "\(APIs.baseURL)getduplicatecheckinoutinfo")!,timeoutInterval: Double.infinity)
        request.addValue(ProjectHelpers.getToken(), forHTTPHeaderField: "x-api-key")
        request.addValue("ci_session=0c04aace0b7340723bb87c1d33e41cab1977c150", forHTTPHeaderField: "Cookie")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
          print(String(data: data, encoding: .utf8)!)
            
            
            print("wtha the fucccckckkkkkkkkkkk")
            print(String(data: data, encoding: .utf8)!)
            
            do{
                let myData: DuplicateModel = try! JSONDecoder().decode(DuplicateModel.self, from: data)
                self.thisDuplicateData = myData
                
//                if myData.data != nil{
//                    self.thisArray = myData.data!.checkInOutDat
//                }else{
//                    self.thisArray.removeAll()
//                }
                
                print("im a success")
            }catch let err{
                print(err)
            }
                   
            DispatchQueue.main.async {
                self.y=1
                
                //MARK: Change
            
                self.behindScreen.isHidden = false
                self.duplicateView.isHidden = false
                self.dummyView.isHidden = false
                self.viewWillLayoutSubviews()
               // self.thisTBV.isHidden = true
//                self.view.backgroundColor = UIColor.lightText
                self.duplicateEntryTBV.reloadData()
            }
            
            
            
            
            
            
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()

        
        
        
        
        
    }
    
    
    
    
    func dupDataJSON(date : String = "",studentID : String){
        
        
        let params = [
            "date" : date,
            "student_id" : studentID,
        ]
        
        
        print("These are the parameters")
        print(params)
        
        print("These are the parameters")
        //APIs.uploadImagesAssignment
        
        JSONManager.postApiWithMultipleImage(APIs.duplicateInfo,type: .post, imageArr: [], imageName: "images[]", Vc: self,parameters: params) { (model:DuplicateModel ) in
           print(model)
            
            
            
            self.thisDuplicateData = model
            //self.thisData = model
            if model.status == true{
                
                
                if self.thisData.data != nil{
                    self.tempArray = self.thisData.data!.checkInOutData!
                    self.thisArray = self.thisData.data!.checkInOutData!
                }else{
                    self.tempArray.removeAll()
                    self.thisArray.removeAll()
                }
                
                
         DispatchQueue.main.async {
             self.y=1
             
             //MARK: Change
         
             self.behindScreen.isHidden = false
             self.duplicateView.isHidden = false
             self.dummyView.isHidden = false
            // self.thisTBV.isHidden = true
//                self.view.backgroundColor = UIColor.lightText
             self.duplicateEntryTBV.reloadData()
         }
                
                
                
            
                
                print("im a success")
            }else{
                
            print("this is false model is coming")
                
            }
            
          //  self.submitAssignmentJson()
            
        }
    
    }
    
    
    
    func hithit(date : String = ""){
        
        
        let params = [
            "date" : date,
            "class" : classID,
        ]
        
        
        print("These are the parameters")
        print(params)
        
        print("These are the parameters")
        //APIs.uploadImagesAssignment
        
        JSONManager.postApiWithMultipleImage(APIs.checkoutHistoryAPI,type: .post, imageArr: [], imageName: "images[]", Vc: self,parameters: params) { (model:OutInModel ) in
           print(model)
            
            self.thisData = model
            if model.status == true{
                
                
                if self.thisData.data != nil{
                    
                    let percentage : Double = 0 //(Double(studentInfoScanned)/Double(studentInfoTotal))*100
                    self.totalCounteeLabel.text = "0/\(self.thisData.data!.total_count_student!)"
                   // cell.percentage = percentage
                    
                    print("This is the percentage :: \(percentage) ::")
                    
                    self.thisLineView.lineFunction(percentage: percentage, color: UIColor(rgb: 0x032DA1), otherColor: UIColor(rgb: 0xDFEBF1))
                  //  return cell
                    
                    
                    
                    
                    if let xc = self.thisData.data!.checkInOutData{
                        self.tempArray = self.thisData.data!.checkInOutData!
                        self.thisArray = self.thisData.data!.checkInOutData!
                        let stData = self.thisData.data!
                        let percentage : Double = (Double(stData.totalStudentScanned!)/Double(stData.totalStudent!))*100
                        
                        
                        
                        self.totalCounteeLabel.text = "\(stData.totalStudentScanned!)/\(stData.totalStudent!)"
                       // cell.percentage = percentage
                        
                        print("This is the percentage :: \(percentage) ::")
                        
                        self.thisLineView.lineFunction(percentage: percentage, color: UIColor(rgb: 0x032DA1), otherColor: UIColor(rgb: 0xDFEBF1))
                        
                        
                        
                        self.x=1
                    }else{
                        self.x=0
                        self.tempArray.removeAll()
                        self.thisArray.removeAll()
                    }
                    
                   
                }else{
                    self.x=0
                    self.tempArray.removeAll()
                    self.thisArray.removeAll()
                }
                
                
                DispatchQueue.main.async {
                   
                    
                    //MARK: Change
                    

                    
                    print("this is the array")
                    print(self.thisArray)
                    print("this is the array")
                    
                    self.thisTBV.reloadData()
                }
                
                
                print("im a success")
            }else{
                
                DispatchQueue.main.async {
                    self.x=0
                    
                    //MARK: Change
                    self.tempArray.removeAll()
                    self.thisArray.removeAll()

                    
                    print("this is the array")
                    print(self.thisArray)
                    print("this is the array")
                    
                    self.thisTBV.reloadData()
                }
                
            }
            
          //  self.submitAssignmentJson()
            
        }
    
    }
    
    
    
    
    
    
    
    
    func postData(date : String = ""){
       
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "date",
            "value": date,
            "type": "text"
          ],
          [
            "key": "class",
            "value": classID,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try! NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://dev.ezydemo.com/api/getcheckinoutinfo")!,timeoutInterval: Double.infinity)
        request.addValue("35edf23cee051eb45849c7514d536cd3", forHTTPHeaderField: "x-api-key")
        request.addValue("ci_session=0c04aace0b7340723bb87c1d33e41cab1977c150", forHTTPHeaderField: "Cookie")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
          print(String(data: data, encoding: .utf8)!)
            
            
            print("wtha the fucccckckkkkkkkkkkk")
            print(String(data: data, encoding: .utf8)!)
            
            do{
                let myData: OutInModel = try! JSONDecoder().decode(OutInModel.self, from: data)
                self.thisData = myData
                
                if myData.data != nil{
                    self.thisArray = myData.data!.checkInOutData!
                }else{
                    self.thisArray.removeAll()
                }
                
                print("im a success")
            }catch let err{
                print(err)
            }
                   
            DispatchQueue.main.async {
                self.x=1
                
                //MARK: Change
                

                
                print("this is the array")
                print(self.thisArray)
                print("this is the array")
                
                self.thisTBV.reloadData()
            }
            
            
            
            
            
            
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()

        
        
        
        
        
    }
    
    
    
}







// MARK: - Welcome
struct OutInModel: Codable {
    let status: Bool
    let data: OutInModelinside?
    let message: String
}

// MARK: - DataClass
struct OutInModelinside: Codable {
    let totalStudent, totalStudentScanned : Int?
    let checkInOutData: [CheckInOutDatum]?

    let total_count_student : String?
    enum CodingKeys: String, CodingKey {
        case totalStudent = "total_student"
        case totalStudentScanned = "total_student_scanned"
        case checkInOutData,total_count_student
    }
}

// MARK: - CheckInOutDatum
struct CheckInOutDatum: Codable {
    let studentID, name, checkInOutDatumIn, out: String
    let duplicateEntry: String

    enum CodingKeys: String, CodingKey {
        case studentID = "student_id"
        case name
        case checkInOutDatumIn = "in"
        case out
        case duplicateEntry = "duplicate_entry"
    }
}




struct DuplicateModel: Codable {
    let status: Bool
    let data: DuplicateModelInside
    let message: String
}

// MARK: - DataClass
struct DuplicateModelInside: Codable {
    let duplicateCheckInOutData: [DuplicateModelInsideOne]
}

// MARK: - DuplicateCheckInOutDatum
struct DuplicateModelInsideOne: Codable {
    let studentID, name, duplicateCheckInOutDatumIn, out: String

    enum CodingKeys: String, CodingKey {
        case studentID = "student_id"
        case name
        case duplicateCheckInOutDatumIn = "in"
        case out
    }
}
