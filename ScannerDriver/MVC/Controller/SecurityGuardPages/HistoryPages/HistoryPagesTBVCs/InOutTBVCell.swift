//
//  InOutTBVCell.swift
//  ScannerDriver
//
//  Created by vinnu subhash on 26/08/21.
//

import UIKit

class InOutTBVCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var inlabel: UILabel!
    @IBOutlet weak var outLabel: UILabel!
    
    @IBOutlet weak var thisImage: UIImageView!
    
    var thisClosure : (()->())!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    
}
