//
//  ProfilePageVC.swift
//  ScannerDriver
//
//  Created by vinnu subhash on 25/08/21.
//

import UIKit
import Alamofire
import SDWebImage

class ProfilePageVC: UIViewController {

    
    @IBOutlet weak var thisTBV: UITableView!
    var x=0
    var thisData : ProfileData!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
    }

    func setup(){
        
        tableviewSetUp(tableView: thisTBV)
        
    }
    
    
    @IBAction func logoutAction(_ sender: UIButton) {
        
        
        let vc = loginStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        vc.modalPresentationStyle = .fullScreen
       // self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
       // self.present(vc, animated: true)
        
    }
    

}
extension ProfilePageVC : UITableViewDelegate,UITableViewDataSource
{
    
    func tableviewSetUp(tableView : UITableView)  {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 280
        tableView.rowHeight = UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        let size = view.frame.size
        return 650
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
      return x
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        thisTBV.register(UINib(nibName: "ProfilePageTBVCell", bundle: nil), forCellReuseIdentifier: "ProfilePageTBVCell")
        let cell = thisTBV.dequeueReusableCell(withIdentifier: "ProfilePageTBVCell") as! ProfilePageTBVCell

        
        
        let da = thisData.data
        
        cell.userIDLabel.text = da.email
        cell.nameLabel.text = da.name
        cell.contactNo.text = da.contactNumber
        cell.genderLabel.text = da.gender
        cell.addressLabel.text = da.address
        let url = URL(string: da.imageURL)
        cell.profileImage.sd_setImage(with: url)
        
       
        
        
        
        
        
        
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == 2{
//            let vc = moreStoryboard.instantiateViewController(withIdentifier: "MyGradesVC") as! MyGradesVC
//            //self.present(vc, animated: true)
//            self.navigationController?.pushViewController(vc, animated: true)
        }
        
  
    }
}



extension ProfilePageVC{
    
    //MARK: Json Data
    
    
    
    func getData()
    {
        
        let apiKey = ProjectHelpers.getToken()
        
        let headers: HTTPHeaders = [
            "x-api-key": apiKey
        ]
        
    //    let params : [:] = [
    //    ]
        
        
        print("URL is")
        print(APIs.baseURL+APIs.profileAPI)
        print("URL is")
        print("This is the api key")
        print(apiKey)
        print("This is the api key")
        
        
        NetworkManager.hitApi(url: APIs.baseURL+APIs.profileAPI, vc: self, paramaters:[:],header: headers) { (model:ProfileData) in
            print(model)
          //  self.tableviewSetUp(tableView: self.thisTBV)
            
            self.thisData = model
            
            if model.status == true{
                self.x=1
            }else{
                self.x=0
                
            }
            
            
            DispatchQueue.main.async { [self] in
              //  self.tableviewSetUp(tableView: self.thisTBV)
                self.thisTBV.reloadData()
            }
        } failure: { e in
            print(e)
        }
    }
    
}




import Foundation

// MARK: - Welcome
struct ProfileData: Codable {
    let status: Bool
    let data: ProfileDataInside
    let message: String
}

// MARK: - DataClass
struct ProfileDataInside: Codable {
    let  name, email, contactNumber: String
    let dob, gender, address: String
    let imageURL: String
//securityID,
    enum CodingKeys: String, CodingKey {
      //  case securityID = "security_id"
        case name, email
        case contactNumber = "contact_number"
        case dob, gender, address
        case imageURL = "image_url"
    }
}





