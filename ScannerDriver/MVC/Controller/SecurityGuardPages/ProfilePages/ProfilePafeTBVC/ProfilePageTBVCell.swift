//
//  ProfilePageTBVCell.swift
//  ScannerDriver
//
//  Created by vinnu subhash on 25/08/21.
//

import UIKit

class ProfilePageTBVCell: UITableViewCell {

    
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var userIDLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var contactNo: UILabel!
    
    @IBOutlet weak var dateOfBirthLAbel: UILabel!
    
    @IBOutlet weak var genderLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setup(){
        profileImage.borderWidth = 3
        profileImage.borderColor = .appThemeColor
        profileImage.cornerRadiusMethod(Int(profileImage.frame.width)/2)
        
    }
    
}
