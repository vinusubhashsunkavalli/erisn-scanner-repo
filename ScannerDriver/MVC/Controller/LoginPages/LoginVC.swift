//
//  LoginVC.swift
//  ScannerDriver
//
//  Created by vinnu subhash on 25/08/21.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var appNameImage: UIImageView!
    
    @IBOutlet weak var appLogo: UIImageView!
    @IBOutlet weak var loginTypeLabel: UILabel!
    
    @IBOutlet weak var userTF: UITextField!
    @IBOutlet weak var passwordTf: UITextField!
    
    @IBOutlet weak var forgotPasswordOut: UIButton!
    
    @IBOutlet weak var eyeIconOut: UIButton!
    
    @IBOutlet weak var loginOut: UIButton!
    
    @IBOutlet weak var selctSchoolOut: UIButton!
    
    
    var isPasswordSeen = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        selctSchoolOut.cornerRadiusMethod(8)
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
//        userTF.text = "ravi@gmail.com"
//        passwordTf.text = "Test@123"
        
        userTF.text = "Raju@blackboard.com"
        passwordTf.text = "Test@123"
         
        super.viewWillAppear(true)
        schoolSetup()
        setup()
        //self.navigationController?.isNavigationBarHidden = false
    }
    
    
    func schoolSetup(){
        
        let size = appNameImage.frame.size
        appNameImage.image = nil
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        label.font = .fontMediumFunc(size: 25)
        label.textColor = .gray
        label.textAlignment = .center
       
        appNameImage.addSubview(label)
        
        appLogo.image = nil
        
        label.text = UserDefaults.standard.string(forKey: UserdefaultsConstants.schoolNameDefaults)!
        let imgURL = URL(string: ProjectHelpers.getLogoString() )
        appLogo.sd_setImage(with:imgURL)
        
    }
    
    
    
    
    @IBAction func selctSchoolAction(_ sender: UIButton) {
        
        UserDefaults.standard.set(false, forKey: UserdefaultsConstants.isSchoolSelected)
        
        let vc = schoolStoryboard.instantiateViewController(withIdentifier: "SchoolListScreen") as! SchoolListScreen
        vc.modalPresentationStyle = .fullScreen
       // self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    func setup(){
        
        userTF.autocorrectionType = .no
        passwordTf.autocorrectionType = .no
        
        loginOut.cornerRadiusMethod(25, opacity: 0.1)
        
        
        
        let defaults = UserDefaults.standard
        if let user = defaults.value(forKey: UserdefaultsConstants.userType){
          
            if user as! String == "Driver"{
                loginTypeLabel.text = "Driver"
                print("Hi driver")
            }else {
                loginTypeLabel.text = "Security Guard"
                print("Hi Scanner")
            }
            
            loginTypeLabel.text! += "Login"
            
            
        }
        
        loginTypeLabel.text = "Security Login"
        
        forgotPasswordOut.isHidden = true
        
    }
    
    
    @IBAction func loginAction(_ sender: UIButton) {
        
      
        
        jsonLogin()
//        let vc = securityGuardStory.instantiateViewController(withIdentifier: "CustomTabbarVC") as! CustomTabbarVC
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true)
        //self.navigationController?.pushViewController(vc, animated: true)
        
    
        
    }
    
    @IBAction func eyeAction(_ sender: UIButton) {
        isPasswordSeen.toggle()
        
        if isPasswordSeen{
            eyeIconOut.setImage(UIImage(named:"closeEyeIcon" ), for: .normal)
        }else{
            eyeIconOut.setImage(UIImage(named: "openEyeIcon"), for: .normal)
        }
        
        
        passwordTf.isSecureTextEntry = isPasswordSeen
        
    }
    
    //MARK: JSON Login
    func jsonLogin()
    {
        
//
//        url = [url, stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
       // var originalString = "test/test"
        
        let url = URL(string:APIs.baseURL+APIs.loginAPI )
//
        
//        print("this is the escaped string")
//        print(escapedString)
//        print("this is the escaped string")
        let parameters = [
            "email" : userTF.text!,
            "password" : passwordTf.text!
        ];
        
        print("These are parameters : \(parameters)")
//        let headers: HTTPHeaders = [
//            "x-api-key":"d88dac9098b82c2dc4c5cf7888ef83d3"
//        ]
        
        NetworkManager.hitApi(url: APIs.baseURL+APIs.loginAPI,vc:self, paramaters: parameters) { (model:LoginModel) in
            
            
            if model.status == true{
                print("****************************")
                print(model.message)
                print(model.data!.token!)
                
                let defaults = UserDefaults.standard
                defaults.set(model.data!.token, forKey: UserdefaultsConstants.tokenID)
                defaults.set(self.userTF.text!, forKey: UserdefaultsConstants.userID)
                
                print("****************************")
                print(model)
                print("****************************")
    
                
                
                  let vc = securityGuardStory.instantiateViewController(withIdentifier: "CustomTabbarVC") as! CustomTabbarVC
                  vc.modalPresentationStyle = .fullScreen
                  self.present(vc, animated: true)
                
                
                
//                let vc = homeStoryboard.instantiateViewController(withIdentifier: "ThisTabbarVC") as! ThisTabbarVC
//                vc.modalPresentationStyle = .overCurrentContext
              //  self.present(vc, animated: true)
                
              //  self.presentDetail(vc)
                
            }else{
                
                
                
                self.showAlert(for: "\(model.message)")
                
                
            }
            
            
            print(model);
            
            
            
        } failure: { e in
            print(e);
        }
 
    }
    
    
    

}




//MARK: LoginModal

struct LoginModel :Codable {
    let status : Bool
    let message : String
    let data : LoginInsideModel?
}

struct LoginInsideModel : Codable{
    
    let token : String?
    
}
