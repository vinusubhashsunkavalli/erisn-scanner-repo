//
//  SelectUserTypeVC.swift
//  ScannerDriver
//
//  Created by vinnu subhash on 25/08/21.
//

import UIKit
import DropDown

class SelectUserTypeVC: UIViewController {

    @IBOutlet weak var appLogo: UIImageView!
    @IBOutlet weak var userDropDownOut: UIButton!
    
    
    let dropDown = DropDown()

    // The view to which the drop down will appear on
     // UIView or UIBarButtonItem

    // The list of items to display. Can be changed dynamically
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        dropSetup()
    }
    
    

    func dropSetup(){
        DropDown.appearance().setupCornerRadius(0) // available since v2.3.6
        
        dropDown.anchorView = userDropDownOut
        dropDown.backgroundColor = UIColor(rgb: 0xF8F8F8)
        dropDown.shadowColor = .white
       // dropDown.cellHeight = .white
        
        
        let arrayType = ["Security Guard","Driver"]
        dropDown.dataSource = arrayType
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        
        let defaults = UserDefaults.standard
        
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
        print("Selected item: \(item) at index: \(index)")
           
            defaults.setValue("\(item)", forKey: UserdefaultsConstants.userType)
        dropDown.hide()
        let vc = loginStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        
        userDropDownOut.semanticContentAttribute = .forceRightToLeft
        userDropDownOut.setImage(UIImage(named: "dropAroowIcon"), for: .normal)
        userDropDownOut.setTitle("Select", for: .normal)
        
        userDropDownOut.titleEdgeInsets = UIEdgeInsets(top: 0, left: -userDropDownOut.frame.width/2-20 , bottom: 0, right: 0 )
        userDropDownOut.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: -userDropDownOut.frame.width/2-40)
        
        userDropDownOut.titleLabel?.font = UIFont(name: Theme.appRegularFont, size: 18)
        
        
        userDropDownOut.borderWidth = 0.5
        userDropDownOut.backgroundColor = .white
        userDropDownOut.borderColor = UIColor(rgb: 0x707070)
        
       // userDropDownOut.cornerRadius = 3
        userDropDownOut.cornerRadiusMethod(5, opacity: 0.0, offx: 0, offy: 0, sahdowColor: .black)
        
    }
    
    
    
    @IBAction func userSelectDropAction(_ sender: UIButton) {
        
        dropDown.show()
        
        var type = dropDown.selectedItem
        print(type)
        
       
        
    }
    
    enum UserType{
        
        case driver
        case securityGuard
        
    }
    
    

}
