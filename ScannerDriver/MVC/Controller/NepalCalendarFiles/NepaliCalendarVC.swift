//
//  NepaliCalendarVC.swift
//  ThisNepaliCalendar
//
//  Created by VinuSubhash Sunkavalli on 04/05/22.
//

import UIKit

class NepaliCalendarVC: UIViewController {

    
    @IBOutlet weak var thisTBV: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("Nepali  calendar vc is printed")
        
        thisTBV.register(NepaliTBVC.self)
        thisTBV.delegate = self
        thisTBV.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    

 

}

extension NepaliCalendarVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        
        return 1

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // let cell = thisTBV.dequeueReusableCell(withIdentifier: "NepaliTBVC") as! NepaliTBVC
        let cell = thisTBV.dequeueReusableCell(withIdentifier: NepaliTBVC.reuseid, for: indexPath) as! NepaliTBVC
        
        
        cell.layoutIfNeeded()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CellViewController") as! CellViewController

        self.addChild(vc)
        cell.contentView.addSubview(vc.view)

        vc.view.translatesAutoresizingMaskIntoConstraints = false
        cell.contentView.addConstraint(NSLayoutConstraint(item: vc.view, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cell.contentView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1.0, constant: 0.0))
        cell.contentView.addConstraint(NSLayoutConstraint(item: vc.view, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cell.contentView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1.0, constant: 0.0))
        cell.contentView.addConstraint(NSLayoutConstraint(item: vc.view, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cell.contentView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0.0))
        cell.contentView.addConstraint(NSLayoutConstraint(item: vc.view, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cell.contentView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0.0))

        vc.didMove(toParent: self)
        vc.view.layoutIfNeeded()
        
        
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    
    
    
}



